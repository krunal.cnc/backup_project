/* Window Load functions */

$(window).load(function(){
    /* animate */     
    setTimeout(function(){
        $('.banner h1').each(function(){
            $(this).html($(this).text().replace(/([^\x00-\x80]|\w)/g, "<span class='letter'>$&</span>"));
        });

        anime.timeline({loop:0}).add({
            targets: '.banner h1 .letter',
            translateX: [40,0],
            translateZ: 0,
            opacity: [0,1],
            easing: "easeOutExpo",
            duration: 500,
            delay: function(el, i) {
                return 500 + 30 * i;
            }
        });

        $('.banner h1').css('opacity','1');
    },600);
    setTimeout(function(){
        $('.banner .inner img').addClass('active') ;
    },500);
    setTimeout(function(){
        $('.loder').fadeOut(300) ;
    },50);

    setTimeout(function(){
        $('.slider_popup').fadeIn(500);
    },3500);


});


$(document).ready(function(){

    $('.store .inner .col .img_col .heart').click(function(){
        $(this).toggleClass('active');
    });

    $('.hamburger').click(function(){
        $(this).toggleClass('active'); 
        $('header .right_col nav').toggleClass('active'); 
    });
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.backtotop').fadeIn();
        } else {
            $('.backtotop').fadeOut();
        }
    });

    $('.backtotop').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

    $('.loder .inner span').addClass('active'); 
    setTimeout(function(){
        $('.loder .inner span').addClass('rota');         
    },700);
    $('.slider_popup').fadeOut();
    $('.slider_popup .wrap>.close').click(function(){
        $('.slider_popup').addClass('active');
    });



});

$(window).resize(function(){

})
$(window).scroll(function(){
    var sticky = $('header'),
        scroll = $(window).scrollTop();

    if (scroll >= 50) sticky.addClass('fixed');
    else sticky.removeClass('fixed');
});

// WAVES EFFECT //
!function(t){"use strict";function e(t){return null!==t&&t===t.window}function n(t){return e(t)?t:9===t.nodeType&&t.defaultView}function a(t){var e,a,i={top:0,left:0},o=t&&t.ownerDocument;return e=o.documentElement,void 0!==t.getBoundingClientRect&&(i=t.getBoundingClientRect()),a=n(o),{top:i.top+a.pageYOffset-e.clientTop,left:i.left+a.pageXOffset-e.clientLeft}}function i(t){var e="";for(var n in t)t.hasOwnProperty(n)&&(e+=n+":"+t[n]+";");return e}function o(t){if(!1===d.allowEvent(t))return null;for(var e=null,n=t.target||t.srcElement;null!==n.parentElement;){if(!(n instanceof SVGElement||-1===n.className.indexOf("waves"))){e=n;break}if(n.classList.contains("waves")){e=n;break}n=n.parentElement}return e}function r(e){var n=o(e);null!==n&&(c.show(e,n),"ontouchstart"in t&&(n.addEventListener("touchend",c.hide,!1),n.addEventListener("touchcancel",c.hide,!1)),n.addEventListener("mouseup",c.hide,!1),n.addEventListener("mouseleave",c.hide,!1))}var s=s||{},u=document.querySelectorAll.bind(document),c={duration:750,show:function(t,e){if(2===t.waves)return!1;var n=e||this,o=document.createElement("div");o.className="waves-ripple",n.appendChild(o);var r=a(n),s=t.pageY-r.top,u=t.pageX-r.left,d="scale("+n.clientWidth/100*15+")";"touches"in t&&(s=t.touches[0].pageY-r.top,u=t.touches[0].pageX-r.left),o.setAttribute("data-hold",Date.now()),o.setAttribute("data-scale",d),o.setAttribute("data-x",u),o.setAttribute("data-y",s);var l={top:s+"px",left:u+"px"};o.className=o.className+" waves-notransition",o.setAttribute("style",i(l)),o.className=o.className.replace("waves-notransition",""),l["-webkit-transform"]=d,l["-moz-transform"]=d,l["-ms-transform"]=d,l["-o-transform"]=d,l.transform=d,l.opacity="1",l["-webkit-transition-duration"]=c.duration+"ms",l["-moz-transition-duration"]=c.duration+"ms",l["-o-transition-duration"]=c.duration+"ms",l["transition-duration"]=c.duration+"ms",l["-webkit-transition-timing-function"]="cubic-bezier(0.250, 0.460, 0.450, 0.940)",l["-moz-transition-timing-function"]="cubic-bezier(0.250, 0.460, 0.450, 0.940)",l["-o-transition-timing-function"]="cubic-bezier(0.250, 0.460, 0.450, 0.940)",l["transition-timing-function"]="cubic-bezier(0.250, 0.460, 0.450, 0.940)",o.setAttribute("style",i(l))},hide:function(t){d.touchup(t);var e=this,n=(e.clientWidth,null),a=e.getElementsByClassName("waves-ripple");if(!(a.length>0))return!1;var o=(n=a[a.length-1]).getAttribute("data-x"),r=n.getAttribute("data-y"),s=n.getAttribute("data-scale"),u=350-(Date.now()-Number(n.getAttribute("data-hold")));u<0&&(u=0),setTimeout(function(){var t={top:r+"px",left:o+"px",opacity:"0","-webkit-transition-duration":c.duration+"ms","-moz-transition-duration":c.duration+"ms","-o-transition-duration":c.duration+"ms","transition-duration":c.duration+"ms","-webkit-transform":s,"-moz-transform":s,"-ms-transform":s,"-o-transform":s,transform:s};n.setAttribute("style",i(t)),setTimeout(function(){try{e.removeChild(n)}catch(t){return!1}},c.duration)},u)},wrapInput:function(t){for(var e=0;e<t.length;e++){var n=t[e];if("input"===n.tagName.toLowerCase()){var a=n.parentNode;if("i"===a.tagName.toLowerCase()&&-1!==a.className.indexOf("waves"))continue;var i=document.createElement("i");i.className=n.className+" waves-input-wrapper";var o=n.getAttribute("style");o||(o=""),i.setAttribute("style",o),n.className="waves-waves-input",n.removeAttribute("style"),a.replaceChild(i,n),i.appendChild(n)}}}},d={touches:0,allowEvent:function(t){var e=!0;return"touchstart"===t.type?d.touches+=1:"touchend"===t.type||"touchcancel"===t.type?setTimeout(function(){d.touches>0&&(d.touches-=1)},500):"mousedown"===t.type&&d.touches>0&&(e=!1),e},touchup:function(t){d.allowEvent(t)}};s.displayEffect=function(e){"duration"in(e=e||{})&&(c.duration=e.duration),c.wrapInput(u(".waves")),"ontouchstart"in t&&document.body.addEventListener("touchstart",r,!1),document.body.addEventListener("mousedown",r,!1)},s.attach=function(e){"input"===e.tagName.toLowerCase()&&(c.wrapInput([e]),e=e.parentElement),"ontouchstart"in t&&e.addEventListener("touchstart",r,!1),e.addEventListener("mousedown",r,!1)},t.Waves=s,document.addEventListener("DOMContentLoaded",function(){s.displayEffect()},!1)}(window);

(function($){

    "use strict";

    // DETECT TOUCH DEVICE //
    function is_touch_device() {
        return !!('ontouchstart' in window) || ( !! ('onmsgesturechange' in window) && !! window.navigator.maxTouchPoints);
    }
    // MULTILAYER PARALLAX //
    function multilayer_parallax() {

        $(".multilayer-parallax .parallax-layer").each(function(){

            var x = parseInt($(this).attr("data-x"), 10),
                y = parseInt($(this).attr("data-y"), 10);

            $(this).css({
                "left": x + "%",
                "top": y + "%"
            });

        });

    }

    // PARALLAX //
    if (typeof $.fn.stellar !== 'undefined') {

        // MULTILAYER PARALLAX //
        multilayer_parallax();

        if (!is_touch_device()) {

            $(window).stellar({
                horizontalScrolling: false,
                verticalScrolling: true,
                responsive: true
            });

        } else {

            $(".parallax").addClass("parallax-disable");

        }

    }
})(window.jQuery);





(function($) {
    $.fn.visible = function(partial) {
        var $t            = $(this),
            $w            = $(window),
            viewTop       = $w.scrollTop(),
            viewBottom    = viewTop + $w.height(),
            _top          = $t.offset().top,
            _bottom       = _top + $t.height(),
            compareTop    = partial === true ? _bottom : _top,
            compareBottom = partial === true ? _top : _bottom;

        return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
    };
})(jQuery);
var win = $(window);
var allMods = $(".about>*");
var allMods1 = $(".blog .right_col>*");
var allMods2 = $(".store .inner .col");
var allMods3 = $(".gold .inner>*");
var allMods4 = $(".blog .left_col");

win.scroll(function(event) {
    allMods.each(function(i, el) {
        var el = $(el);
        if (el.visible(true)) {
            setTimeout(function(){
                el.addClass("come-in"); 
            },i*200);
        } 
    });
});
win.scroll(function(event) {
    allMods1.each(function(i, el) {
        var el = $(el);
        if (el.visible(true)) {
            setTimeout(function(){
                el.addClass("come-in"); 
            },i*50);
        } 
    });
});
win.scroll(function(event) {
    allMods2.each(function(i, el) {
        var el = $(el);
        if (el.visible(true)) {
            setTimeout(function(){
                el.addClass("come-in"); 
            },i*50);
        } 
    });
});
win.scroll(function(event) {
    allMods3.each(function(i, el) {
        var el = $(el);
        if (el.visible(true)) {
            setTimeout(function(){
                el.addClass("come-in"); 
            },i*100 );
        } 
    });
});

win.scroll(function(event) {
    allMods4.each(function(i, el) {
        var el = $(el);
        if (el.visible(true)) {
            setTimeout(function(){
                el.addClass("come-in"); 
            },i*100 );
        } 
    });
});


/* inner page tab */
$('#tabContent2').fadeOut(500);
$('.inner_wrapper ul.nav-tabs li a').click(function (e) {

    var tabContent = '#tabContent' + this.id; 
    $('#tabContent2').fadeOut(500);
    $('#tabContent1').fadeOut(500);
    $(tabContent).fadeIn(500);
    $('.inner_wrapper ul.nav-tabs li a').removeClass('active');
    $(this).addClass('active');

    if($(this).attr('id') == '1')
    { 
        var update_text="Privacy policy"
        $('.inner_page .banner h2').html(update_text).fadeIn(1000);
    }
    else{
        var update_text="Terms & conditions"
        $('.inner_page .banner h2').html(update_text).fadeIn(1000);
    }

})
