/* Window Load functions */
$(window).load(function(){
    setTimeout(function(){
        $('.banner .bg').addClass('load_done');
    },50);
    setTimeout(function(){
        $('header').addClass('load_done');
    },200); 
    setTimeout(function(){
        $('.banner h1').addClass('load_done');
    },400);
    setTimeout(function(){
        $('.banner .search_col').addClass('load_done');
    },700);

    if($(window).width() < 1024 ){
        $('header nav').height($(window).height() - 120);
    }
    if($(window).width() < 1024 ){
        setTimeout(function(){
            $('header nav ul li.drop .nav-submenu').slideUp();
            $('header nav ul li.drop>.nav-submenu>li .inner_submenu').slideUp();
            $('header nav ul li.drop>.nav-submenu>li .inner_submenu .sub_menu_site_link>li .right_sub_link').slideUp();
            $('header nav ul li.drop>.nav-submenu1').slideUp();

        },50);                                        
    }
    var r = $('.dispensaries .main_tab .tab_contant_list .tab_content').eq(0).outerHeight();
    $(".dispensaries .main_tab .tab_contant_list").outerHeight(r);

});

$(document).ready(function(){
    if($(window).width() < 1024 ){
        $('header nav>ul>li.dr2>a').click(function(){
            $(this).toggleClass('active');
            $(this).next().slideToggle();            
        });
        $('header nav>ul>li.dr1>a,header nav>ul>li.dr3>a').click(function(){
            $(this).toggleClass('active');
            $(this).next().slideToggle();            
        });
        $('header nav ul li.drop>.nav-submenu>li>a').click(function(){
            $(this).toggleClass('active');
            $(this).next().slideToggle();            
        });
        $('header nav ul li.drop>.nav-submenu>li .inner_submenu .sub_menu_site_link>li>a').click(function(){
            $(this).toggleClass('active');
            $(this).next().slideToggle();            
        });
    }
    var highestBox = 0;
    $('.weedoc .col').each(function(){  
        if($(this).height() > highestBox){  
            highestBox = $(this).height();  
        }
    });    
    $('.weedoc .col').height(highestBox);

    $('.hamburger').click(function(){
        $(this).toggleClass('active');
        $('.ready').toggleClass('active');

    });
    $(window).scroll(function(){
        var sticky = $('header'),
            scroll = $(window).scrollTop();

        if (scroll >= 50) sticky.addClass('fixed');
        else sticky.removeClass('fixed');
    });

    var r = $('.dispensaries .main_tab .tab_contant_list .tab_content').eq(0).outerHeight();
    $(".dispensaries .main_tab .tab_contant_list").outerHeight(r);
    $(".dispensaries .main_tab .title_tab ul li").click(function() {
        var i =$('.dispensaries .main_tab .title_tab ul li').index(this);
        var r =$('.dispensaries .main_tab .tab_contant_list .tab_content').eq(i).outerHeight();
        $(".dispensaries .main_tab .tab_contant_list").height(r);
        $(".dispensaries .main_tab .title_tab ul li").removeClass('active');
        $(this).addClass('active');
        $(".dispensaries .main_tab .tab_contant_list .tab_content").removeClass('active');  
        $(".dispensaries .main_tab .tab_contant_list .tab_content").eq(i).addClass('active');  
    });
    var slider2 = $("#slider1");
    $('#slider1').owlCarousel({
        loop:true,                   
        nav : true,
        /* center:true,*/
        margin:34,
        autoWidth:true,
        items:1,
    });   

});
/* Tab */
$(window).load(function(){
    var r = $('.tab .tab-content').eq(0).outerHeight();
    $(".tab").outerHeight(r);
})
var r = $('.tab .tab-content').eq(0).outerHeight();
$(".tab").outerHeight(r);
$(".tabs-menu a").click(function() {
    var i = $('.tabs-menu a').index(this);
    var r = $('.tab-content').eq(i).outerHeight();
    $(".tab").height(r);
    $(".tabs-menu a").parent().removeClass('active');
    $(this).parent().addClass('active');
    $(".tab-content").removeClass('active');  
    $(".tab-content").eq(i).addClass('active');  
});

/* star */
$('.shop_info .left .menu_list .tab-content .rating_top .star > i').click(function() {
    $(this).addClass('active').siblings().removeClass('active');
    $(this).parent().attr('data-rating-value', $(this).data('rating-value'));
});

$(window).load(function(){
    function progress(percent, $element) {
        var progressBarWidth = percent * $element.width() / 25;
        $element.find('div').animate({ width: progressBarWidth }, 500);
        $('#compare_price .shop_price_co .top .right .p1 h6').html(percent + "% ");        
    }
    function progress2(percent, $element) {
        var progressBarWidth = percent * $element.width() / 25;
        $element.find('div').animate({ width: progressBarWidth }, 500);
        $('#compare_price .shop_price_co .top .right .p2 h6').html(percent + "% ");        
    }
    progress(19.6, $('#progressBar'));
    progress2(0.62, $('#progressBar2'));
});


$('.shop_info .left .menu_list .tab_contener .tab-content .inner_nav ul li a[href*=#]').click(function(event){
    $('html, body').animate({
        scrollTop: $( $.attr(this, 'href') ).offset().top
    }, 500);
    event.preventDefault();
});

$('#compare_price .back_top').click(function(e){
    e.preventDefault();   
    var a = $(this).attr('href');

    $('html,body').animate({
        scrollTop: $(a).offset().top 
    },800);    
});

/*sticky*/
/*$(document).ready(function(){
    $(window).scroll(function(){
        if ($(window).scrollTop() >= 10) {
            $('header').addClass('sticky');
        }
        else {
            $('header').removeClass('sticky');
        }
    });
});*/


$('.shop_info .left .title_shop .shop_address .loc a').click(function(){
    $('.map_popup').addClass('show');
});

$('.map_popup .map_con .close').click(function(){
    $('.map_popup').removeClass('show');
});




