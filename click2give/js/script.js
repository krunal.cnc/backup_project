/* Window Load functions */

$(window).on('load',function(){
    setTimeout(function(){

    });
});


$(document).ready(function(){
    $('.collapse .accordion').click(function(){
        if($(this).hasClass('active'))
        {
            $(this).removeClass('active');
            setTimeout( function(){
                $(this).parents('.collapse').removeClass('active');
            },1000)
            $(this).next('.panel').slideUp(500);
        }
        else{
            $('.accordion').removeClass('active');
            $('.collapse').removeClass('active');
            $('.panel').slideUp(500);
            $(this).parents('.collapse').addClass('active');
            $(this).addClass('active');
            $(this).next('.panel').slideDown(500);
        }

    });
});

$(window).scroll(function(){
    if ($(window).scrollTop() >= 20) {
        $('header').addClass('sticky');
    }
    else {
        $('header').removeClass('sticky');
    }
});

/* bubble animation */
var radius = 8;
TweenMax.staggerFromTo('.c_ani', 4 ,{
    cycle: {
        attr:function(i) {
            var r = i*90;
            return {
                transform:'rotate('+r+') translate('+radius+',0.1) rotate('+(-r)+')'
            }      
        }
    }  
},{
    cycle: {
        attr:function(i) {
            var r = i*90+360;
            return {
                transform:'rotate('+r+') translate('+radius+',0.1) rotate('+(-r)+')'
            }      
        }
    },
    ease:Linear.easeNone,
    repeat:-1
});

/* How It's work svg aniamtion */
var radius_path92 = 10;
TweenMax.staggerFromTo('.Path_92', 3 ,{
    cycle: {
        attr:function(i) {
            var r = i*90;
            return {
                transform:'rotate('+r+') translate('+radius_path92+',0.1) rotate('+(-r)+')'
            }      
        }
    }  
},{
    cycle: {
        attr:function(i) {
            var r = i*90+360;
            return {
                transform:'rotate('+r+') translate('+radius_path92+',0.1) rotate('+(-r)+')'
            }      
        }
    },
    ease:Linear.easeNone,
    repeat:-1
});


/*responsiveSlides*/
$("#rslides").responsiveSlides({
    speed: 500,            
    timeout: 5000,          
    pager: true,           
    random: false, 
    pause: false,           
    pauseControls: true,  
    namespace: "rslides",   
    before: function(){},   
    after: function(){}     
});
/* achievement */
equalheight = function(container){
    var currentTallest = 0,
        currentRowStart = 0,
        rowDivs = new Array(),
        $el,
        topPosition = 0;
    $(container).each(function() {

        $el = $(this);
        $($el).height('auto')
        topPostion = $el.position().top;

        if (currentRowStart != topPostion) {
            for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
            rowDivs.length = 0; // empty the array
            currentRowStart = topPostion;
            currentTallest = $el.height();
            rowDivs.push($el);
        } else {
            rowDivs.push($el);
            currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
        }
        for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
            rowDivs[currentDiv].height(currentTallest);
        }
    });
}
$(window).load(function() {
    equalheight('.our_solution .service_blog .blog');
});
$(window).resize(function(){
    equalheight('.our_solution .service_blog .blog');
});
/* custome select box */
$('.contact_sec .con_wrap .co_form select').each(function(){
    var $this = $(this), numberOfOptions = $(this).children('option').length;
    $this.addClass('select-hidden'); 
    $this.wrap('<div class="select"></div>');
    $this.after('<div class="select-styled"></div>');

    var $styledSelect = $this.next('div.select-styled');
    $styledSelect.text($this.children('option').eq(0).text());

    var $list = $('<ul />', {
        'class': 'select-options'
    }).insertAfter($styledSelect);

    for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
            text: $this.children('option').eq(i).text(),
            rel: $this.children('option').eq(i).val()
        }).appendTo($list);
    }

    var $listItems = $list.children('li');

    $styledSelect.click(function(e) {
        e.stopPropagation();
        $('div.select-styled.active').not(this).each(function(){
            $(this).removeClass('active').next('ul.select-options').hide();
        });
        $(this).toggleClass('active').next('ul.select-options').toggle();
    });

    $listItems.click(function(e) {
        e.stopPropagation();
        $styledSelect.text($(this).text()).removeClass('active');
        $this.val($(this).attr('rel'));
        $list.hide();
        //console.log($this.val());
    });

    $(document).click(function() {
        $styledSelect.removeClass('active');
        $list.hide();
    });
});


function owl_carousel(){
    $('.what_are_doing .raising_money .rising_slider').owlCarousel({
        loop:true,                   
        items:1,
        nav:false,
        dots:true,
        autoplay:true,
        smartSpeed:500,
        drag:false,
        touchDrag: false,
        mouseDrag: false,
        rtl: true,
        animateOut: 'fadeOut'
    }); 
}

/* come in animation */
jQuery(document).ready(function(){
    $(function(){
        var $elems = $('.animation');
        var winheight = $(window).height();
        var fullheight = $(document).height();
        $(window).scroll(function(){
            animate_elems();
        });
        $(window).on('load',function(){
            animate_elems();
        })
        function animate_elems() {
            wintop = $(window).scrollTop(); // calculate distance from top of window

            // loop through each item to check when it animates
            $elems.each(function(){
                $elm = $(this);

                if($elm.hasClass('come-in')) { return true; } // if already animated skip to the next item

                topcoords = $elm.offset().top; // element's distance from top of page in pixels

                if(wintop > (topcoords - (winheight*.65))) {
                    // animate when top of the window is 3/4 above the element
                    $elm.addClass('come-in');
                }
            });
        }
    });


    (function($) {
        $.fn.visible = function(partial) {
            var $t            = $(this),
                $w            = $(window),
                viewTop       = $w.scrollTop(),
                viewBottom    = viewTop + $w.height(),
                _top          = $t.offset().top,
                _bottom       = _top + $t.height(),
                compareTop    = partial === true ? _bottom : _top,
                compareBottom = partial === true ? _top : _bottom;

            return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
        };
    })(jQuery);
    var win = jQuery(window);
    var allMods1 = jQuery(".what_doing h2");
    var allMods2 = jQuery(".what_doing p");
    var allMods3 = jQuery(".what_doing h6");
    var allMods4 = jQuery(".our_solution .service_blog .blog");
    var allMods5 = jQuery(".our_solution .service_include .icon_list .mi_text");
    var allMods6 = jQuery(".our_solution .service_include .icon_list .icon");
    var allMods7 = jQuery(".faq .faq_wrap .taq_tab .collapse");
    var allMods8 = jQuery(".what_are_doing .raising_money .what_text h2");
    var allMods9 = jQuery(".what_are_doing .raising_money .what_text p");
    var allMods10 = jQuery(".what_are_doing .raising_money .what_text h6");

    win.scroll(function(event) {
        allMods1.each(function(i, el) {
            var el = jQuery(el);
            if (el.visible(true)) {
                setTimeout(function(){
                    if(el.hasClass('hidden')){
                        el.removeClass('hidden');					
                        el.addClass("show"); 
                    }else{
                        el.addClass("show"); 
                    }
                },i*200);
            } 
        });
    });    
    win.scroll(function(event) {
        allMods2.each(function(i, el) {
            var el = jQuery(el);
            if (el.visible(true)) {
                setTimeout(function(){
                    if(el.hasClass('hidden')){
                        el.removeClass('hidden');					
                        el.addClass("show"); 
                    }else{
                        el.addClass("show"); 
                    }
                },i*200);
            } 
        });
    });    
    win.scroll(function(event) {
        allMods3.each(function(i, el) {
            var el = jQuery(el);
            if (el.visible(true)) {
                setTimeout(function(){
                    if(el.hasClass('hidden')){
                        el.removeClass('hidden');					
                        el.addClass("show"); 
                    }else{
                        el.addClass("show"); 
                    }
                },i*200);
            } 
        });
    });
    win.scroll(function(event) {
        allMods4.each(function(i, el) {
            var el = jQuery(el);
            if (el.visible(true)) {
                setTimeout(function(){
                    if(el.hasClass('hidden')){
                        el.removeClass('hidden');					
                        el.addClass("show"); 
                    }else{
                        el.addClass("show"); 
                    }
                },i*200);
            } 
        });
    });
    win.scroll(function(event) {
        allMods5.each(function(i, el) {
            var el = jQuery(el);
            if (el.visible(true)) {
                setTimeout(function(){
                    if(el.hasClass('hidden')){
                        el.removeClass('hidden');					
                        el.addClass("show"); 
                    }else{
                        el.addClass("show"); 
                    }
                },i*200);
            } 
        });
    });
    win.scroll(function(event) {
        allMods6.each(function(i, el) {
            var el = jQuery(el);
            if (el.visible(true)) {
                setTimeout(function(){
                    if(el.hasClass('hidden')){
                        el.removeClass('hidden');					
                        el.addClass("show"); 
                    }else{
                        el.addClass("show"); 
                    }
                },i*200);
            } 
        });
    });
    win.scroll(function(event) {
        allMods7.each(function(i, el) {
            var el = jQuery(el);
            if (el.visible(true)) {
                setTimeout(function(){
                    if(el.hasClass('hidden')){
                        el.removeClass('hidden');					
                        el.addClass("show"); 
                    }else{
                        el.addClass("show"); 
                    }
                },i*200);
            } 
        });
    });
    win.scroll(function(event) {
        allMods8.each(function(i, el) {
            var el = jQuery(el);
            if (el.visible(true)) {
                setTimeout(function(){
                    if(el.hasClass('hidden')){
                        el.removeClass('hidden');					
                        el.addClass("show"); 
                    }else{
                        el.addClass("show"); 
                    }
                },i*200);
            } 
        });
    });
    win.scroll(function(event) {
        allMods9.each(function(i, el) {
            var el = jQuery(el);
            if (el.visible(true)) {
                setTimeout(function(){
                    if(el.hasClass('hidden')){
                        el.removeClass('hidden');					
                        el.addClass("show"); 
                    }else{
                        el.addClass("show"); 
                    }
                },i*200);
            } 
        });
    });
    win.scroll(function(event) {
        allMods10.each(function(i, el) {
            var el = jQuery(el);
            if (el.visible(true)) {
                setTimeout(function(){
                    if(el.hasClass('hidden')){
                        el.removeClass('hidden');					
                        el.addClass("show"); 
                    }else{
                        el.addClass("show"); 
                    }
                },i*200);
            } 
        });
    });
});

/* popup */
$(document).on('click','.banner .banner_text .let_start',function(){
    $('.popup').addClass('active')
});
$(document).on('click','.pop_close',function(){
    $('.popup').removeClass('active')
});




/*  mobile  menu */

if ($('.mobile_menu').length > 0) {
    (function(window) {
        'use strict';
        function classReg(className) {
            return new RegExp("(^|\\s+)" + className + "(\\s+|$)");
        }
        var hasClass, addClass, removeClass;
        if ('classList' in document.documentElement) {
            hasClass = function(elem, c) {
                return elem.classList.contains(c);
            };
            addClass = function(elem, c) {
                elem.classList.add(c);
            };
            removeClass = function(elem, c) {
                elem.classList.remove(c);
            };
        } else {
            hasClass = function(elem, c) {
                return classReg(c).test(elem.className);
            };
            addClass = function(elem, c) {
                if (!hasClass(elem, c)) {
                    elem.className = elem.className + ' ' + c;
                }
            };
            removeClass = function(elem, c) {
                elem.className = elem.className.replace(classReg(c), ' ');
            };
        }

        function toggleClass(elem, c) {
            var fn = hasClass(elem, c) ? removeClass : addClass;
            fn(elem, c);
        }
        var classie = {
            hasClass: hasClass,
            addClass: addClass,
            removeClass: removeClass,
            toggleClass: toggleClass,
            has: hasClass,
            add: addClass,
            remove: removeClass,
            toggle: toggleClass
        };
        if (typeof define === 'function' && define.amd) {
            define(classie);
        } else {
            window.classie = classie;
        }
    })(window);
    (function() {
        var bodyEl = document.body,
            content = document.querySelector('.content-wrap'),
            openbtn = document.getElementById('open-button'),
            closebtn = document.getElementById('close-button'),
            isOpen = false,
            morphEl = document.getElementById('morph-shape'),
            s = Snap(morphEl.querySelector('svg'));
        path = s.select('path');
        initialPath = this.path.attr('d'), steps = morphEl.getAttribute('data-morph-open').split(';');
        stepsTotal = steps.length;
        isAnimating = false;

        function init() {
            initEvents();
        }

        function initEvents() {
            openbtn.addEventListener('click', toggleMenu);
            if (closebtn) {
                closebtn.addEventListener('click', toggleMenu);
            }
        }

        function toggleMenu() {
            if (isAnimating) return false;
            isAnimating = true;
            if (isOpen) {
                classie.remove(bodyEl, 'show-menu');
                setTimeout(function() {
                    path.attr('d', initialPath);
                    isAnimating = false;
                }, 300);
            } else {
                classie.add(bodyEl, 'show-menu');
                var pos = 0,
                    nextStep = function(pos) {
                        if (pos > stepsTotal - 1) {
                            isAnimating = false;
                            return;
                        }
                        path.animate({
                            'path': steps[pos]
                        }, pos === 0 ? 400 : 500, pos === 0 ? mina.easein : mina.elastic, function() {
                            nextStep(pos);
                        });
                        pos++;
                    };
                nextStep(pos);
            }
            isOpen = !isOpen;
        }
        init();
    })();
}




/* on scroll smooth scroll */
$(document).ready(function () {
    $(document).on("scroll", onScroll);
    $('header .bottom .menu nav ul li a[href^="#"]').on('click', function (e) {
        e.preventDefault();
        $(document).off("scroll");

        $('a').each(function () {
            $(this).removeClass('active');
        })
        $(this).addClass('active');

        var target = this.hash,
            menu = target;
        $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top - (jQuery('header').height()) //If Any additional height then minus it
        }, 500, 'swing', function () {
            $(document).on("scroll", onScroll);
        });
    });
});

function onScroll(event){
    var scrollPos = $(document).scrollTop();
    $('header .bottom .menu nav ul li a').each(function () {
        var count_rm = (jQuery('header').height()); //If Any additional height then minus it
        var currLink = $(this);
        var refElement = $(currLink.attr("href"));
        if ((refElement.position().top - count_rm) <= scrollPos && (refElement.position().top - count_rm) + refElement.height() > scrollPos) {
            $('header .bottom .menu nav ul li a').removeClass("active");
            currLink.addClass("active");
        }
        else{
            currLink.removeClass("active");
        }
    });
}



$('header .bottom .menu .home_icon,.banner .scroll_to,.contact_sec .con_wrap .scroll_bottom').click(function() {
    var targetDiv = $(this).attr('href');
    $('html, body').animate({
        scrollTop: $(targetDiv).offset().top - ($('header').height())
    },500);
});



