/* Window Load functions */
$(window).load(function(){
	$('body').addClass('show_content')

	if($('.what_page').length > 0){
		$('body').fadeIn(1000);
		setTimeout(function(){
			$('.menu').addClass('active_link');
			$('.logo_wrapper').addClass('small');

			setTimeout(function(){
				$('.what_content').addClass('active');
				$('.logo_wrapper.small .logo h1').addClass('small');
				$('.button_border').addClass('active_white');
				$('.login').addClass('small');
			},100)
			setTimeout(function(){
				if($('.live_performance').length > 0){
					/*$('.what_content .what_wrapper .live_performance').addClass('show');
					$('.what_content .what_wrapper .live_performance .title_logo h3').addClass('show');
					$('.what_content .what_wrapper .close_popup').addClass('show');    */
				}
			},1000)
		},100)
	}
});


$(document).ready(function(){
	setTimeout(function(){
		$('body').addClass('show_content')
	},2000)
});

$(window).resize(function(){

})

/* Navigation */
$('.menu ul li a,.logo_link,.what_content .icon .popup').click(function(evt){
	console.log('Page Change')
	if($(this).attr('href') != "javascript:void(0)" && $(this).attr('href') != undefined && $(this).attr('href') != null){
		evt.preventDefault()
		$('body').removeClass('show_content')
		var href = $(this).attr('href')
		setTimeout(function(){
			console.log(href) 
			window.location = href
		},1000)
	}

});


/*-----------------------------index page----------------------------------------*/
/* menu click */
$('.what').click(function(){
	setTimeout(function(){
		$('.menu').addClass('active_link');
		$('.logo_wrapper').addClass('small');
	},2000);
	setTimeout(function(){
		$('.overlay.loading').removeClass('active')
		/*setTimeout(function(){
            window.location = "what.html";
        },550)*/
	},1000)
});


$('.play').click(function(){
	/*$('.button_border').toggleClass('active');*/
	var url_string = $('iframe').attr('src'); //window.location.href
	var url = new URL(url_string);
	var c = url.searchParams.get("autoplay");
	url_string = url_string.replace('autoplay=0','autoplay=1')
	$('iframe').attr('src',url_string);
	var i;
	for (i = 1; i < 60; i++) {
		$('.button_border').css('transform','scale('+i+')')
	}
	$('.overlay').toggleClass('fade_out');
	//$('#my-video').addClass('view');
	$(this).hide()
	setTimeout(function(){
		$('iframe').addClass('show')
		$('#stop').addClass('show')
	},1000)
});

$('#my-video').click(function(){
	setTimeout(function(){
		$('.overlay').removeClass('fade_out');
	},1000)
	$('.button_border').removeClass('active');
	$(this).removeClass('view');
});

$('.overlay.fade_out').click(function(){
	$('.button_border').removeClass('active');
	$('.overlay').removeClass('fade_out');
	console.log(0)
});

$(window).on('load',function(){
	$('.logo_wrapper .logo').addClass('active');
	$('.logo_wrapper .logo h1').addClass('active');

	/* index page */
	setTimeout(function(){
		$('.menu ul').addClass('active');
		$('.login').addClass('active');
		$('.logo_wrapper .logo h4').addClass('active');

		setTimeout(function(){
			$('.menu ul li a').addClass('active');	
		},1000);
	},1000);

	/* contact page */
});


/*-----------------------------what page----------------------------------------*/
/* custome scroll bar */
$(document).ready(function(){
	$(window).on("load",function(){
		$(".scroll_content").mCustomScrollbar({
			theme:"dark",
			callbacks:{
				onScroll:function(x){
					$('.pop_scroll').show()   
				},
				onTotalScroll:function(x){ 
					$('.pop_scroll').hide()
				}
			}
		});
	});
});

/* on click popup show */
$('.what_content .icon .popup svg').click(function(){
	$(this).next().next('.live_performance').addClass('show');
	$(this).next().next('.live_performance .title_logo h3').addClass('show');
});

$('.live_performance .close_popup').click(function(){
	$('.what_content .what_wrapper .live_performance').removeClass('show');
	$('.what_content .what_wrapper .close_popup').removeClass('show');
});



/*-----------------------------contact page----------------------------------------*/
/* close contact popup */
/*$('.pop_over').click(function(){
    $('.overlay .logo_wrapper .contact .partners .partners_icon .client').removeClass('popup-show');
    $('.pop_over').removeClass('popup-overlay');
});*/


/* contact popup */

/*$('.overlay .logo_wrapper .contact .partners .partners_icon').click(function(){
    var clicks = $(this).data('clicks');
    if (clicks) {
        $('.overlay .logo_wrapper .contact .partners .partners_icon .client').removeClass('popup-show');
        $('.overlay .logo_wrapper .contact .partners .partners_icon').removeClass('active');	
    }
    else{		
        $(this).children().addClass('popup-show');
        $('.pop_over').addClass('popup-overlay');
        $(this).addClass('active');	
    }
    $(this).data("clicks", !clicks);
});*/



/*-----------------------------contact page----------------------------------------*/
/* close contact popup */
$('.pop_over').click(function(){
	$('.partners').removeClass('active');
	$('.overlay .logo_wrapper .contact .partners .partners_icon .client').slideUp();
	$('.pop_over').removeClass('popup-overlay');
	$('.overlay .logo_wrapper .contact .partners .partners_icon').removeClass('active');
});

/* contact popup */
$('.overlay .logo_wrapper .contact .partners .partners_icon').click(function(){
	if(!$(this).hasClass('active')){
		$('.partners_icon').removeClass('active')
		$('.overlay .logo_wrapper .contact .partners .partners_icon .client').slideUp();
		$(this).addClass('active');
		setTimeout(function(){
			$('.partners.show').addClass('active');
		},150)
		$(this).children('.client').slideDown();
		$('.pop_over').addClass('popup-overlay');    
	}
	else{
		//        $('.partners_icon').removeClass('active')
		//        $('.overlay .logo_wrapper .contact .partners .partners_icon .client').slideUp();
		//        $(this).removeClass('active');
	}
});

$(document).on('click','.overlay .logo_wrapper .contact .partners .partners_icon .client,.partners.show.active',function(){
	$('.overlay .logo_wrapper .contact .partners .partners_icon').removeClass('active')
	$('.partners').removeClass('active')
	$('.overlay .logo_wrapper .contact .partners .partners_icon .client').slideUp()
	$('.pop_over').removeClass('popup-overlay');	
});



/*-----------------------------When page----------------------------------------*/

/* Date Scroll Section */
if($(window).width() >1024){
	$(function() {
		if($('.calcnder_wrapper').length > 0){
			$(".calcnder_wrapper").mousewheel(function(event, delta) {
				if($(this).hasClass('active')){
					this.scrollTop -= (delta * 25);
				}
				else{
					this.scrollLeft -= (delta * 25);
				}
				event.preventDefault();
			}); 
		}
	}); 
}
else{
}


/* Date Detail */
$('.calcnder_wrapper .inner_cont .dates').click(function(){
	if($(this).children().hasClass('detail_section')){
		if($('.calcnder_wrapper .inner_cont .dates.active').length > 0){
			$('.calcnder_wrapper').removeClass('active')    
			/*$('.calcnder_wrapper').animate({scrollTop:0}, 400);*/
			$('.overlay_full_white').removeClass('white_layer');	
		}
		else{
			if(!$(this).hasClass('pink') && !$(this).hasClass('yellow')){
				$('.calcnder_wrapper').animate({scrollTop:0}, 400);
			}
			else{
				$('.calcnder_wrapper').animate({scrollTop:0}, 100);
			}
			$('.overlay_full_white').addClass('white_layer');	
		}
		$(this).toggleClass('active');
	}
	$('.overlay_full_white').addClass('white_layer');	
	if($(window).width() < 960){
		if($('.calcnder_wrapper .inner_cont .dates.active').length > 0){
			$('.outer_wrapper').addClass('active')
		}
		else{
			$('.outer_wrapper').removeClass('active');
		}

	}
	else{
		$(this).children('.detail_section').slideToggle();
	}


});

/* close when page popup on outer click  */
/*$(document).on('click','.overlay_full_white.white_layer',function(){
	$(this).removeClass('white_layer');
});*/

/* Opne Mobile POPUP */
$('.date_list a').click(function(){
	$('.white_overlay').addClass('active');
});

$('.close').click(function(){
	setTimeout(function(){
		$('.overlay_full_white').removeClass('white_layer')
	},100)
});



$(window).load(function(){
	/* Set Offset */
	$('.calcnder_wrapper .inner_cont .dates').each(function(){
		$(this).attr('data-offset',$(this).offset().left)
	});
	$('.date_list a').each(function(){
		$(this).attr('data-offset',$(this).offset().top)
	});
	/* Scrollanimation */
	$('.calcnder_wrapper').animate({scrollLeft:$('.calcnder_wrapper').width()/4}, 750);

	/* Set Page screenFit*/
	if($(window).width() > 1024){
		if($('.when_cont').length > 0)
			$('.main_section').css('padding-top',$(window).outerHeight() - ($('.when_cont').outerHeight() + $('.bottom_footer').outerHeight()));
	}
})

$(window).resize(function(){
	/* Set Page screenFit*/
	setTimeout(function(){
		if($(window).width() > 1024){
			if($('.when_cont').length > 0)
				$('.main_section').css('padding-top',$(window).outerHeight() - ($('.when_cont').outerHeight() + $('.bottom_footer').outerHeight()));
		}
	},250)
})

/* Date Scroll */
var scroll_ammountl;
if($(window).width() > 1660){
	scroll_ammountl = 350;
}
else if($(window).width() <= 1660 && $(window).width() > 1440){
	scroll_ammountl = 280;
}
else if($(window).width() <= 1440 && $(window).width() >= 1300){
	scroll_ammountl = 280;
}



$('.mount_section ul li a').click(function(){
	if(!$(this).hasClass('active')){
		$('.mount_section ul li a').removeClass('active')
		$(this).addClass('active')
		var _id = $(this).attr('id')
		if($(window).width() > 960){
			$('.calcnder_wrapper .inner_cont .dates').each(function(){
				var _x = $(this).attr('data-calander');
				if(_x == _id){
					var _offset = $(this).attr('data-offset');
					console.log(scroll_ammountl)
					$('.calcnder_wrapper').animate({scrollLeft:(_offset - scroll_ammountl)});
				}
			})  
		}
		else{
			$('.mob_section .date_list a').each(function(){
				var _x = $(this).attr('data-calander');
				var _offset = $(this).attr('data-offset')
				if(_x == _id){
					$('.date_list').animate({scrollTop:_offset - 350 });
				}
			})  
		}
	}
})

/*---------------------------Mobile slider------------------------------*/
/*---------------------------------------mobile contact slider---------------------------------------------*/
function owl(){
	if($(window).width() <= 767){
		if ($("#owl-demo").hasClass("owl-carousel")) {
			return false;
		}
		else{
			$('#owl-demo').addClass('owl-carousel')
			$('#owl-demo').addClass('owl-theme')
			$("#owl-demo").owlCarousel({
				singleItem:true,
				loop:false,
				margin:0,
				items: 1,
				nav:false,
				rtl: false,
				dots:false
			});
		}

	}
	else{
		if($("#owl-demo").children().hasClass('owl-stage-outer'))
		{
			console.log('0')
			$("#owl-demo").trigger('destroy.owl.carousel').removeClass('owl-carousel owl-loaded owl-theme');
			$("#owl-demo").find('.owl-stage-outer').children().unwrap();
		}
		else{
			return false;   
		}
	}
}
if($('#owl-demo').length > 0){
	$(window).resize(owl);
	$(window).ready(owl);    
}

var _count_ = 1;
$('.our_talent .col').each(function(){
	if(_count_ == 4)
		_count_ = 1;
	$(this).addClass('col_'+_count_)
	_count_++
})


var iframe = $('iframe')[0];
if(iframe){
	var player = $f(iframe);
	$('#stop').click(function() {
		console.log('vidoe stop')
		player.api('pause');
		$('.overlay').removeClass('fade_out');
		$('iframe').removeClass('show')    
		$(this).removeClass('show')
		$('#my-video').removeClass('view');
		setTimeout(function(){
			$('.button_border').removeClass('active');
		},2000)

		var i;
		for (i = 60; i > 0; i--) {
			$('.button_border').css('transform','scale('+i+')')
		}
		setTimeout(function(){
			$('.play').show()
		},2500)

	});   
}


/*---------------------------------Am chart map page----------------------------------------------*/
/*class=""*/
var map = AmCharts.makeChart( "chartdiv", {
	"type": "map",
	"theme": "light",
	"mouseWheelZoomEnabled": true,
	"showTitleonOnHover": false,
	"panEventsEnabled":true,
	"dataProvider": {
		"map": "worldHigh",
		"zoomLevel": 1.2, 
		"zoomLongitude": 10.1400926,
		"zoomLatitude": 50.8476655,
		"areas": [ {
			"title": "United Kingdom",
			"id": "GB",
			"color": "#df021c",
			"customData": "1995",
			"groupId": "before2004"
		},{
			"title": "Germany",
			"id": "DE",
			"color": "#df021c",
			"customData": "1957",
			"groupId": "before2004"
		},{
			"title": "Finland",
			"id": "FI",
			"color": "#df021c",
			"customData": "1957",
			"groupId": "before2004"
		},{
			"title": "Spain",
			"id": "ES",
			"color": "#df021c",
			"customData": "1957",
			"groupId": "before2004"
		}],
		"images": [ {
			"id": "id1",
			"latitude": 51.501466,
			"longitude": -0.6001,
			"imageURL": "images/site/marker.png",
			"height": 100,"zoomLevel": 5,"groupId": "minZoom-2.5",
			"width": 100,
			"height": 100,
			"title": "<span>london heathrow airport terminal 5</span>",
			"modalUrl": "http://www.contentainment.com/wp-content/uploads/pdf/heathrow.pdf",

		}, {
			"id": "id2", 
			"latitude": 51.4578652,
			"longitude": -0.8099584,	
			"imageURL": "images/site/marker.png",
			"width": 100,
			"height": 100,"zoomLevel": 5,"groupId": "minZoom-2.5",
			"title": "<span>london heathrow airport terminal 4</span>",
			"modalUrl": "http://www.contentainment.com/wp-content/uploads/pdf/heathrow.pdf",
		}, {
			"id": "id3",
			"latitude": 51.6012667,
			"longitude": -0.4576137,
			"imageURL": "images/site/marker.png",
			"width": 100,
			"height": 100,"zoomLevel": 5,"groupId": "minZoom-2.5",
			"title": "<span>london heathrow airport terminal 3</span>",
			"modalUrl": "http://www.contentainment.com/wp-content/uploads/pdf/heathrow.pdf",

		}, {
			"id": "id4",
			"latitude": 51.4695422,
			"longitude": -0.3004002,
			"imageURL": "images/site/marker.png",
			"width": 100,
			"height": 100,"zoomLevel": 5,"groupId": "minZoom-2.5",
			"title": "<span>london heathrow airport terminal 2</span>",
			"modalUrl": "http://www.contentainment.com/wp-content/uploads/pdf/heathrow.pdf",

		}, {
			"id": "id5",
			"latitude": 51.8860181,
			"longitude": -0.2366774,
			"imageURL": "images/site/marker.png",
			"width": 100,
			"height": 100,"zoomLevel": 5,"groupId": "minZoom-2.5",
			"title": "<span>London Stansted</span>",
			"modalUrl": "http://www.contentainment.com/wp-content/uploads/pdf/heathrow.pdf",

		}, {
			"id": "id6",
			"latitude": 51.157543,
			"longitude": -0.1641477,
			"imageURL": "images/site/marker.png",
			"width": 100,
			"height": 100,"zoomLevel": 5,"groupId": "minZoom-2.5",
			"title": "<span>London Gatwick South</span>",
			"modalUrl": "http://www.contentainment.com/wp-content/uploads/pdf/southampton.pdf",

		}, {
			"id": "id7",
			"latitude": 51.1595587,
			"longitude": -0.4504002,
			"imageURL": "images/site/marker.png",
			"width": 100,
			"height": 100,"zoomLevel": 5,"groupId": "minZoom-2.5",
			"title": "<span>London Gatwick North</span>",
			"modalUrl": "http://www.contentainment.com/wp-content/uploads/pdf/gatwick.pdf",

		},{
			"id": "id8",
			"latitude": 55.8690744,
			"longitude": -4.4372416,
			"imageURL": "images/site/marker.png",
			"width": 100,
			"height": 100,"zoomLevel": 5,"groupId": "minZoom-2.5",
			"title": "<span>Glasgow</span>",
			"modalUrl": "http://www.contentainment.com/wp-content/uploads/pdf/glasgow.pdf",


		},{
			"id": "id9",
			"latitude": 53.360952,
			"longitude": -2.2774024,
			"imageURL": "images/site/marker.png",
			"width": 100,
			"height": 100,"zoomLevel": 5,"groupId": "minZoom-2.5",
			"title": "<span>Manchester Terminal 1</span>",
			"modalUrl": "http://www.contentainment.com/wp-content/uploads/pdf/manchester.pdf",

		},{
			"id": "id10",
			"latitude": 52.4774169,
			"longitude": -1.9336706,
			"imageURL": "images/site/marker.png",
			"width": 100,
			"height": 100,"zoomLevel": 5,"groupId": "minZoom-2.5",
			"title": "<span>Birmingham</span>",
			"modalUrl": "http://www.contentainment.com/wp-content/uploads/pdf/edinburgh.pdf",

		},{
			"id": "id11",
			"latitude": 49.2074637,
			"longitude": -2.1974094,
			"imageURL": "images/site/marker.png",
			"width": 100,
			"height": 100,"zoomLevel": 5,"groupId": "minZoom-2.5",
			"title": "<span>Jersey</span>",
			"modalUrl": "http://www.contentainment.com/wp-content/uploads/pdf/jersey.pdf",

		},{
			"id": "id12",
			"latitude": 51.2876146,
			"longitude": 6.7646025,
			"imageURL": "images/site/marker.png",
			"width": 100,
			"height": 100,"zoomLevel": 5,"groupId": "minZoom-2.5",
			"title": "<span>Düsseldorf</span>",
			"modalUrl": "http://www.contentainment.com/wp-content/uploads/pdf/dusseldorf.pdf",  

		},{
			"id": "id13",
			"latitude": 60.3210416,
			"longitude": 24.9506717,
			"imageURL": "images/site/marker.png",
			"width": 100,
			"height": 100,"zoomLevel": 5,"groupId": "minZoom-2.5",
			"title": "<span>Helsinki</span>",
			/*"modalUrl": "http://www.contentainment.com/wp-content/uploads/pdf/helsinki.pdf",*/

		},{
			"id": "id14",
			"latitude": 41.297445,
			"longitude": 2.0811054,
			"imageURL": "images/site/marker.png",
			"width": 100,
			"height": 100,"zoomLevel": 5,"groupId": "minZoom-2.5",
			"title": "<span>Barcelona</span>",
			"modalUrl": "http://www.contentainment.com/wp-content/uploads/pdf/barcelona.pdf",

		},{
			"id": "id15",
			"latitude": 40.4983322,
			"longitude": -3.5697869,
			"imageURL": "images/site/marker.png",
			"width": 100,
			"height": 100,"zoomLevel": 5,"groupId": "minZoom-2.5",
			"title": "<span>Madrid</span>",
			"modalUrl": "http://www.contentainment.com/wp-content/uploads/pdf/madrid.pdf",

		}]
	},
	"imagesSettings": {
		"labelRollOverColor": "#000",
		"labelPosition": "bottom"
	},

	"areasSettings": {
		"rollOverOutlineColor": "#FFFFFF",
		"rollOverColor": "#e92020",
		"alpha": 0.8
	},

	"legend": {
		"width": "100%",
		"marginRight": 27,
		"marginLeft": 27,
		"equalWidths": false,
		"backgroundAlpha": 0.5,
		"backgroundColor": "#FFFFFF",
		"borderColor": "#ffffff",
		"borderAlpha": 1,
		"top": 450,
		"left": 0,
		"horizontalGap": 10,
		"data": [ {
			"color": "#e92020"
		}, {
			"color": "#e92020"
		}, {
			"color": "#e92020"
		}, {
			"color": "#e92020"
		} ]
	},
	"export": {
		"enabled": true
	},

	"responsive": {
		"enabled": true,
		"addDefaultRules": false,
		"rules": [{
			"maxWidth": 600,
		}, {
			"maxWidth": 400,
		}, {
			"maxWidth": 250, 
		}]
	},
	"listeners": [{
		"event": "clickMapObject",
		"method": function(event) {
			/*window.open(event.mapObject.modalUrl, '_blank');*/
			var get_title = event.mapObject.title;
			var site_map = event.mapObject.modalUrl;
			var description = event.mapObject.description;
			$('.map_popup').addClass('map_con');
			$('.map_overlay').addClass('active_map');
			$('.map_popup .right h2').html(get_title);

			$('.map_popup .right a').attr('href',site_map);
			if(!$('.map_popup .right a').attr('href')) { 

				$('.map_popup .right a').css('display','none');
			}
			else{
				$('.map_popup .right a').attr('href',site_map);
				$('.map_popup .right a').css('display','block');
			}

			/*console.log(site_map);*/

		}
	}],
});

function clickObject(id) {
	map.clickMapObject( map.getObjectById(id));
} 


map.addListener( "rendered", function() {
	revealMapImages();
	map.addListener( "zoomCompleted", revealMapImages );
} );


function revealMapImages( event ) {
	var zoomLevel = map.zoomLevel();
	if ( zoomLevel < 2 ) {
		map.hideGroup( "minZoom-2" );
		map.hideGroup( "minZoom-2.5" );
	} else if ( zoomLevel < 2.5 ) {
		map.showGroup( "minZoom-2" );
		map.hideGroup( "minZoom-2.5" );
	} else {
		map.showGroup( "minZoom-2" );
		map.showGroup( "minZoom-2.5" );
	}
}


/* map */
$(window).load(function(){
	setTimeout(function(){
		$('#chartdiv').css('min-height', $(window).height());
	},1000)
});

/* map click close */
$('.map_popup .map_content .close_map').click(function(){
	$('.map_popup').removeClass('map_con');
	$('.map_overlay').removeClass('active_map');
});
$(document).on('click','.map_overlay.active_map',function(){
	$('.map_popup').removeClass('map_con');
	$('.map_overlay').removeClass('active_map');
});


/* on click date center */
$('.calcnder_wrapper .inner_cont .dates').click(function(){

	var _x_  = $('.mount_section').offset().left - ($(this).offset().left + $(this).children('.detail_section').width());
	var _y_ = ($('.calcnder_wrapper .inner_cont').width()/2-$(this).offset().left - $('.calcnder_wrapper .inner_cont').position().left);
	
	console.log(_y_);
	
	$('.calcnder_wrapper .inner_cont').css('pading-left',"("+Math.abs(_y_)+"px)");
	/*$('.calcnder_wrapper .inner_cont').css('pading-left',_y_);*/
	/*$('.calcnder_wrapper .inner_cont').animate({
		scrollLeft:_y_
	},350);*/
		
	
	/*var pl = ($('.calcnder_wrapper').outerWidth()/2) - $(this).offset().left;	
	var l = $(this).offset().left;
	console.log(l);
	if (pl < 0) {
		pl = -pl;
	}
	else{} 
	$('.calcnder_wrapper .inner_cont').css('padding-left',pl);*/
});

$(document).on('click','.overlay_full_white.white_layer',function(){
	$('.overlay_full_white').removeClass('white_layer')
	$('.detail_section').slideUp();
	$('.dates.active').removeClass('active');
	$('.calcnder_wrapper .inner_cont').css('padding-left',150); 
});

$('.popup_section .close').click(function(){
	$('.white_overlay').removeClass('active')
})



/* iframe open */
$('#iframe_open').click(function(){
	$('.login_popup').addClass('show');
	$('.overlay_login').addClass('show');
});

$('.login_popup .iframe_close').click(function(){
	$('.login_popup').removeClass('show'); 
	$('.login_popup').find('iframe').remove(); 
	$('.overlay_login').removeClass('show');

});

$('.overlay_login').click(function(){
	$('.login_popup').removeClass('show'); 
	$('.overlay_login').removeClass('show');
	$('.login_popup').find('iframe').remove(); 
});	


/* popup append */
$('.login').click(function(){
	
});

