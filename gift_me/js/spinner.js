var _createClass = function () {function defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}return function (Constructor, protoProps, staticProps) {if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;};}();function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}var easeInOutQuart = function easeInOutQuart(t, b, c, d) {
  if ((t /= d / 2) < 1) return c / 2 * t * t * t * t + b;
  return -c / 2 * ((t -= 2) * t * t * t - 2) + b;
};var

Wheel = function () {

  function Wheel(s) {_classCallCheck(this, Wheel);

    this.canvas = document.createElement('canvas');
    this.context = this.canvas.getContext('2d');

    this.width = s;
    this.height = s;
    this.radius = s / 2;

    this.started = 0;
    this.elapsed = 0;

    this.duration = 1500;
    
                     

  }_createClass(Wheel, [{ key: 'init', value: function init()

    {
      this.canvas.width = this.width;
      this.canvas.height = this.height;

      this.canvas.style.width = this.width;
      this.canvas.style.height = this.height;
    

      this.started = Date.now();
    } }, { key: 'update', value: function update(

    dt) {
      this.elapsed += dt;

      this.progress = this.elapsed / this.duration;
      this.progress = this.progress > 1 && 1 || this.progress;
    } }, { key: 'render', value: function render()

    {
      var progress = easeInOutQuart(this.progress, 0, 1, 1);

      this.context.clearRect(0, 0, this.width, this.height);

      var offsetX = this.width / 2;
      var offsetY = this.height / 2;

      var radius = Math.max(0, progress * this.radius);
      var angle = progress * Math.PI * 2;

      var x = Math.cos(angle - Math.PI) * (this.radius - radius) + offsetX;
      var y = Math.sin(angle - Math.PI) * (this.radius - radius) + offsetY;

      this.context.fillStyle = '#4effed';
      this.context.strokeStyle = '#4effed';

      this.context.save();

      this.context.beginPath();
      this.context.arc(offsetX, offsetY, this.radius, 0, 2 * Math.PI);
      this.context.fill();

      this.context.globalCompositeOperation = 'source-in';

      var x2 = Math.cos(-angle) * radius;
      var y2 = Math.sin(-angle) * radius;

      this.context.beginPath();
      this.context.arc(offsetX + x2, offsetY - y2, this.radius, angle, angle + Math.PI);
      this.context.lineWidth = radius * 2;
      this.context.stroke();

      this.context.restore();

      this.context.beginPath();
      this.context.arc(x, y, radius, 0, 2 * Math.PI);
      this.context.fill();
    } }]);return Wheel;}();var



Spinner = function () {

  function Spinner(s) {_classCallCheck(this, Spinner);

    this.canvas = document.createElement('canvas');
    this.context = this.canvas.getContext('2d');

    this.width = s;
    this.height = s;

    this.wheels = [];

    this.update = this.update.bind(this);

    document.body.appendChild(this.canvas);

    this.time = 0;

  }_createClass(Spinner, [{ key: 'init', value: function init()

    {
      this.canvas.width = this.width;
      this.canvas.height = this.height;

      this.canvas.style.width = this.width;
      this.canvas.style.height = this.height;

      this.time = Date.now();
    } }, { key: 'start', value: function start()

    {
      this.tick();
      this.update();
    } }, { key: 'tick', value: function tick()

    {
      var wheel = new Wheel(this.width);

      wheel.init();

      this.wheels.push(wheel);
    } }, { key: 'update', value: function update()

    {
      window.requestAnimationFrame(this.update);

      var now = Date.now();
      var dt = now - this.time;

      if (this.wheels.slice(-1)[0].progress === 1) {
        if (this.wheels.length + 1 > 4) {
          this.wheels = [];
        }

        this.tick();
      }

      for (var i = 0, wheel; i < this.wheels.length; i++) {
        wheel = this.wheels[i];
        wheel.update(dt);
      }

      this.time = now;

      this.render();
    } }, { key: 'render', value: function render()

    {
      this.context.clearRect(0, 0, this.width, this.height);
      this.context.globalCompositeOperation = 'source-over';

      for (var i = 0, wheel; i < this.wheels.length; i++) {
        wheel = this.wheels[i];
        wheel.render();

        this.context.save();

        if (!!(i % 2)) {
          this.context.globalCompositeOperation = 'destination-out';
        }
        this.context.drawImage(wheel.canvas, 0, 0, this.width, this.height);

        this.context.restore();
      }

      this.context.globalCompositeOperation = 'destination-in';

      this.context.beginPath();
      this.context.arc(this.width / 2, this.height / 2, this.width / 2 - 3, 0, 2 * Math.PI);
      this.context.fill();
    } }]);return Spinner;}();



var spinner = new Spinner(160);

spinner.init();
spinner.start();var _createClass = function () {function defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}return function (Constructor, protoProps, staticProps) {if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;};}();function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}var easeInOutQuart = function easeInOutQuart(t, b, c, d) {
  if ((t /= d / 2) < 1) return c / 2 * t * t * t * t + b;
  return -c / 2 * ((t -= 2) * t * t * t - 2) + b;
};var

Wheel = function () {

  function Wheel(s) {_classCallCheck(this, Wheel);

    this.canvas = document.createElement('canvas');
    this.context = this.canvas.getContext('2d');

    this.width = s;
    this.height = s;
    this.radius = s / 2;

    this.started = 0;
    this.elapsed = 0;

    this.duration = 1500;
    
                     

  }_createClass(Wheel, [{ key: 'init', value: function init()

    {
      this.canvas.width = this.width;
      this.canvas.height = this.height;

      this.canvas.style.width = this.width;
      this.canvas.style.height = this.height;
    

      this.started = Date.now();
    } }, { key: 'update', value: function update(

    dt) {
      this.elapsed += dt;

      this.progress = this.elapsed / this.duration;
      this.progress = this.progress > 1 && 1 || this.progress;
    } }, { key: 'render', value: function render()

    {
      var progress = easeInOutQuart(this.progress, 0, 1, 1);

      this.context.clearRect(0, 0, this.width, this.height);

      var offsetX = this.width / 2;
      var offsetY = this.height / 2;

      var radius = Math.max(0, progress * this.radius);
      var angle = progress * Math.PI * 2;

      var x = Math.cos(angle - Math.PI) * (this.radius - radius) + offsetX;
      var y = Math.sin(angle - Math.PI) * (this.radius - radius) + offsetY;

      this.context.fillStyle = '#4effed';
      this.context.strokeStyle = '#4effed';

      this.context.save();

      this.context.beginPath();
      this.context.arc(offsetX, offsetY, this.radius, 0, 2 * Math.PI);
      this.context.fill();

      this.context.globalCompositeOperation = 'source-in';

      var x2 = Math.cos(-angle) * radius;
      var y2 = Math.sin(-angle) * radius;

      this.context.beginPath();
      this.context.arc(offsetX + x2, offsetY - y2, this.radius, angle, angle + Math.PI);
      this.context.lineWidth = radius * 2;
      this.context.stroke();

      this.context.restore();

      this.context.beginPath();
      this.context.arc(x, y, radius, 0, 2 * Math.PI);
      this.context.fill();
    } }]);return Wheel;}();var



Spinner = function () {

  function Spinner(s) {_classCallCheck(this, Spinner);

    this.canvas = document.createElement('canvas');
    this.context = this.canvas.getContext('2d');

    this.width = s;
    this.height = s;

    this.wheels = [];

    this.update = this.update.bind(this);

    document.body.appendChild(this.canvas);

    this.time = 0;

  }_createClass(Spinner, [{ key: 'init', value: function init()

    {
      this.canvas.width = this.width;
      this.canvas.height = this.height;

      this.canvas.style.width = this.width;
      this.canvas.style.height = this.height;

      this.time = Date.now();
    } }, { key: 'start', value: function start()

    {
      this.tick();
      this.update();
    } }, { key: 'tick', value: function tick()

    {
      var wheel = new Wheel(this.width);

      wheel.init();

      this.wheels.push(wheel);
    } }, { key: 'update', value: function update()

    {
      window.requestAnimationFrame(this.update);

      var now = Date.now();
      var dt = now - this.time;

      if (this.wheels.slice(-1)[0].progress === 1) {
        if (this.wheels.length + 1 > 4) {
          this.wheels = [];
        }

        this.tick();
      }

      for (var i = 0, wheel; i < this.wheels.length; i++) {
        wheel = this.wheels[i];
        wheel.update(dt);
      }

      this.time = now;

      this.render();
    } }, { key: 'render', value: function render()

    {
      this.context.clearRect(0, 0, this.width, this.height);
      this.context.globalCompositeOperation = 'source-over';

      for (var i = 0, wheel; i < this.wheels.length; i++) {
        wheel = this.wheels[i];
        wheel.render();

        this.context.save();

        if (!!(i % 2)) {
          this.context.globalCompositeOperation = 'destination-out';
        }
        this.context.drawImage(wheel.canvas, 0, 0, this.width, this.height);

        this.context.restore();
      }

      this.context.globalCompositeOperation = 'destination-in';

      this.context.beginPath();
      this.context.arc(this.width / 2, this.height / 2, this.width / 2 - 3, 0, 2 * Math.PI);
      this.context.fill();
    } }]);return Spinner;}();



var spinner = new Spinner(160);

spinner.init();
spinner.start();