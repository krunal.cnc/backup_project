/* Window Load functions */

$(window).load(function(){
    setTimeout(function(){
        $('.page1 h1').addClass('active');
        setTimeout(function(){    
            $('.page1 .man_and_woman').addClass('active');
        },700);
    },1200);

    $('.section_mob .same_click,.section .same_click').click(function(){
        $('#fp-nav ul li a.active').parent('li').next('li').find('a').click();
    });

    setTimeout(function(){        
        $('.loader').fadeOut();
        $('canvas').fadeOut();
    },500);

    if($(window).width() < 768){

        /*$('.page4 .bausing_arrow').removeClass('last');*/
        $('.page4 .bausing_arrow').addClass('same_click');
        $('.page6 .bausing_arrow').addClass('last');

    } else{
        /*$('.page4 .bausing_arrow').addClass('last');*/
        $('.page6 .bausing_arrow').removeClass('last');
    }

    $(document).on('click','.section .last',function(){
        $('#fp-nav ul li:first-child').find('a').click();
    });

    setTimeout(function(){
        $(document).on('click','.section_mob .last',function(){
            $('#fp-nav ul li:first-child').find('a').click();
        });
    },50)
    $('.page4 .same_click').click(function(){
        $('#fp-nav ul li a.active').parent('li').next('li').find('a').click();
    });

});


$(document).ready(function(){
    $('.desk_hambarger').click(function(){
        $(this).toggleClass('active'); 
        $('.right_menu').toggleClass('active');
    });

    /*    $('.page4 .bausing_arrow').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });*/
    $(document).on('click','#fp-nav ul li a',function(e){
        e.preventDefault();
    })

});


$(window).resize(function(){

})

// variables
var isAnimatedSecond = $('.page2')
var isAnimatedthird = $('.page3')
var isAnimatedforth = $('.page4')
var isAnimatedfive = $('.page5')
var isAnimatedfive1 = $('.page5')
var isAnimatedsix = $('.page3_mob1')

$('#fullpage').fullpage({

    navigation: true,
    onLeave: function(index, nextIndex, direction) {

        if( index == 1 && nextIndex == 2 ) { 
            isAnimatedSecond.addClass('animate_page2'); 
        }   
        else if( ( index == 1 || index == 2 ) && nextIndex == 3 ) {
            isAnimatedthird.addClass('animate_page3'); 
        }        
        else if( ( index == 1 || index == 2 || index == 3 ) && nextIndex == 4 ) {
            isAnimatedforth.addClass('animate_page4'); 
        }
        else if( ( index == 1 || index == 2 || index == 3 || index == 4 ) && nextIndex == 5 ) {
            isAnimatedfive.addClass('animate_page5'); 
            setTimeout(function(){
                isAnimatedfive1.addClass('active1');
            },2500);
        }
        else if( ( index == 1 || index == 2 || index == 3 || index == 4 || index == 5 ) && nextIndex == 6 ) {
            isAnimatedsix.addClass('animate_page6'); 
        }


    }

});



/* snow gift animation */
var snowsrc = './images/site/g2.png';
var no = 5;
var hidesnowtime = 0;
var snowdistance = "50"; 
var ie4up = (document.all) ? 1 : 0;
var ns6up = (document.getElementById&&!document.all) ? 1 : 0; 
function iecompattest(){
    return (document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body
}
var dx, xp, yp;
var am, stx, sty;   
var i, doc_width = 200, doc_height = 150; 
if (ns6up) {
    doc_width = self.innerWidth;
    doc_height = self.innerHeight;
} else if (ie4up) {
    doc_width = iecompattest().clientWidth;
    doc_height = iecompattest().clientHeight;
}

dx = new Array();
xp = new Array();
yp = new Array();
am = new Array();
stx = new Array();
sty = new Array();
for (i = 0; i < no; ++ i) {  
    var messages = ["./images/site/g2.png", "./images/site/g1.png", "./images/site/g3.png", "./images/site/g1_left.png", "./images/site/g2_left.png"];
    snowsrc = messages[Math.floor(Math.random() * messages.length)];
    console.log(snowsrc);
    dx[i] = 0;                        
    xp[i] = Math.random()*(doc_width-50);  
    yp[i] = Math.random()*doc_height;
    am[i] = Math.random()*20;         
    stx[i] = 0.02 + Math.random()/10; 
    sty[i] = 0.7 + Math.random();     
    if (ie4up||ns6up) {
        if($(window).width() > 767 ){
            if (i == 0) {
                $('.container').append("<div id=\"dot"+ i +"\" style=\"POSITION: absolute; Z-INDEX: "+ i +"; VISIBILITY: visible; TOP: 15px; LEFT: 15px;\"><img src='"+snowsrc+"' border=\"0\"><\/div>");
            } else {
                $('.container').append("<div id=\"dot"+ i +"\" style=\"POSITION: absolute; Z-INDEX: "+ i +"; VISIBILITY: visible; TOP: 15px; LEFT: 15px;\"><img src='"+snowsrc+"' border=\"0\"><\/div>");
            }

        }
        else {

            if (i == 0) {
                var no = 2;
                var messages1 = ["./images/site/g2.png","./images/site/g2.png","./images/site/g2.png","./images/site/g2.png"];
                snowsrc1 = messages1[Math.floor(Math.random() * messages1.length)];

                $('.container1').append("<div id=\"dot"+ i +"\" style=\"POSITION: absolute; Z-INDEX: "+ i +"; VISIBILITY: visible; TOP: 0px; LEFT: 0px;\"><img src='"+snowsrc1+"' border=\"0\"><\/div>");
            } else {
                $('.container1').append("<div id=\"dot"+ i +"\" style=\"POSITION: absolute; Z-INDEX: "+ i +"; VISIBILITY: visible; TOP: 0px; LEFT: 0px;\"><img src='"+snowsrc1+"' border=\"0\"><\/div>");
            }
        }

    }
}

function snowIE_NS6() {  
    doc_width = ns6up?window.innerWidth-10 : iecompattest().clientWidth-10;
    doc_height=(window.innerHeight && snowdistance=="windowheight")? window.innerHeight : (ie4up && snowdistance=="windowheight")?  iecompattest().clientHeight : (ie4up && !window.opera && snowdistance=="pageheight")? iecompattest().scrollHeight : iecompattest().offsetHeight;
    for (i = 0; i < no; ++ i) {  
        yp[i] += sty[i];
        if (yp[i] > doc_height-50) {
            xp[i] = Math.random()*(doc_width-am[i]-30);
            yp[i] = 0;
            stx[i] = 0.02 + Math.random()/10;
            sty[i] = 0.7 + Math.random();
        }
        dx[i] += stx[i];
        document.getElementById("dot"+i).style.top=yp[i]+"px";
        document.getElementById("dot"+i).style.left=xp[i] + am[i]*Math.sin(dx[i])+"px";  
    }
    snowtimer=setTimeout("snowIE_NS6()", 5);  /* time */
}

function hidesnow(){
    if (window.snowtimer) clearTimeout(snowtimer)
    for (i=0; i<no; i++) document.getElementById("dot"+i).style.visibility="hidden"
}


if (ie4up||ns6up){
    snowIE_NS6();
    if (hidesnowtime>0)
    {
        if($(window).width() > 767 ){
            setTimeout("hidesnow()", hidesnowtime*100)
        }
        else{
            setTimeout("hidesnow()", hidesnowtime*10)
        }

    }
}


/* button color change */
jQuery(document).ready(function(){
    var doc = document.getElementById("background");
    var color = ["#4C61EE", "#EB1D46", "#D94CEE", "#EE884C","#B00A2B","#F597A9"];
    var i = 0;
    function change() {
        doc.style.color = color[i];
        /*$('.page5 .inner .Start_now').css("border-color",color[i]);*/

        i++;

        if(i > color.length - 1) {
            i = 0;
        }
    }
    setInterval(change, 5000);
});
jQuery(document).ready(function(){
    var doc_mob = document.getElementById("background1");
    var color_mob = ["#4C61EE", "#EB1D46", "#D94CEE", "#EE884C","#B00A2B","#F597A9"];
    var i = 0;
    function change_mob() {
        doc_mob.style.color = color_mob[i];
        i++;

        if(i > color_mob.length - 1) {
            i = 0;
        }
    }
    setInterval(change_mob, 3000);
});



/* If device landspace */
getOrientation();

function getOrientation(){
    $('body').removeAttr('class');
    $('body').addClass(90===Math.abs(window.orientation) ? "landscape" : "portrait");
}

window.onorientationchange = function() {
    getOrientation();
};



jQuery(document).ready(function($){
    var deviceAgent = navigator.userAgent.toLowerCase();
    var agentID = deviceAgent.match(/(iPad|iPhone|iPod)/i);
    if (agentID) {     
        window.addEventListener('DOMContentLoaded',function() {
            if($('.ios_gif').length > 0)
            {
                var this_v = $('.ios_gif');
                this_v.append('<img src=""  class="gif_img" alt="">')
                var img_src = this_v.attr('data-img')
                console.log(img_src)
                this_v.find('img').attr('src',img_src);
            }
            else{
                
            }
        });     
    }
    else{

    }
}); 

