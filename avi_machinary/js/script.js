/* Window Load functions */

$(window).load(function(){
    setTimeout(function(){

    });
});

$(window).resize(function(){

})


 $(document).ready(function(){
    $(window).scroll(function(){
    if ($(window).scrollTop() >= 100) {
        $('header').addClass('sticky');
    }
    else {
        $('header').removeClass('sticky');
    }
  });
});

/* tab active */
 $(window).load(function(){
    var r = $('.tab .tab-content').eq(0).outerHeight();
    $(".tab").outerHeight(r);
})
    var r = $('.tab .tab-content').eq(0).outerHeight();
    $(".tab").outerHeight(r);
    $(".tabs-menu a").click(function() {
        var i =$('.tabs-menu a').index(this);
        var r =$('.tab-content').eq(i).outerHeight();
        $(".tab").height(r);
        $(".tabs-menu a").parent().removeClass('active');
        $(this).parent().addClass('active');
        $(".tab-content").removeClass('active');  
        $(".tab-content").eq(i).addClass('active');  
    });
/*back to top */

 $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.back_to_top').fadeIn();
        } else {
            $('.back_to_top').fadeOut();
        }
});
 $('.back_to_top').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 1000);
        return false;
});

/* menu toggle */
$('.hamburger').click(function () {
      $('.hamburger').toggleClass('is-active'); 
      $('.navigation').toggleClass('is-active');   
});

/* mobile dropdown */

$('header .navigation nav>ul .drop,header .container .navigation .catalog').click(function () {
    if($('header .navigation nav>ul .drop .drop_box,header .navigation .catalog .catalog_drop_down').hasClass('dr-active')) 
    {
        $('header .navigation nav>ul .drop .drop_box,header .navigation .catalog .catalog_drop_down').removeClass('dr-active');    
        $(this).removeClass('is-active');
    }
    else{
        $(this).addClass('is-active');
        $(this).children().addClass('dr-active');
    }
});

/* sticky tab */
$(document).ready(function(){
    $(window).scroll(function(){
        if($(window).scrollTop() >= 210){
            $('.outer_full_tab .inner_full_tab').addClass('sticky_tab');
        }   
        else
        {
            $('.outer_full_tab .inner_full_tab').removeClass('sticky_tab');
        }    
    });
});


/* on scroll full_tab change */
$(document).ready(function () {
    $(document).on("scroll", onScroll);
    
    //smoothscroll
    $('.outer_full_tab .inner_full_tab ul li a[href^="#"]').on('click', function (e) {
        e.preventDefault();
        $(document).off("scroll");
        
        $('.outer_full_tab .inner_full_tab ul li a').each(function () {
            $(this).removeClass('full_active');
        })
        $(this).addClass('full_active');
      
        var target = this.hash,
            menu = target;
        $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top - (($('header').height()) + $('.outer_full_tab .inner_full_tab').height())
        }, 500, 'swing', function () {
            /*window.location.hash = target;*/
            $(document).on("scroll", onScroll);
        });
    });
});

function onScroll(event){
    var scrollPos = $(document).scrollTop();
    $('.outer_full_tab .inner_full_tab ul li a').each(function () {
        var currLink = $(this);
        var refElement = $(currLink.attr("href"));
        if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
            $('.outer_full_tab .inner_full_tab ul li a').removeClass("full_active");
            currLink.addClass("full_active");
        }
        else{
            currLink.removeClass("full_active");
        }
    });
}



            