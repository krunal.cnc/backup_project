/* sticky header */
 $(document).ready(function(){
        $(window).scroll(function(){
            if ($(window).scrollTop()) {
                $('header').addClass('sticky');
            }
            else {
                $('header').removeClass('sticky');
            }
        });
        /* fulltab active */
         $('.full_tab ul li a').click(function(){                                                
            if($(this).hasClass('active')){
                    $(this).addClass('active');
                    $(this).next('active').removeClass('active');
            }
            else{
                    $('.full_tab ul li a').removeClass('active');
                    $(this).addClass('active');
            }
        });
        
        /* gallery stickt tab */
         $(window).scroll(function(){
            if($(window).scrollTop() >= 250){
                $('.tab_relative .tab_gallery').addClass('sticky_tab');
            }   
            else
            {
                $('.tab_relative .tab_gallery').removeClass('sticky_tab');
            }    
        });
});
$('.full_tab ul li a').click(function(e){
     e.preventDefault();  
     var a = $(this).attr('href');
     $('html,body').animate({
         scrollTop: $(a).offset().top - 200 
     },1000);    
});
/* back to top */
 $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.back_top').fadeIn();
        } else {
            $('.back_top').fadeOut();
        }
});
 $('.back_top').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 800);
        return false;
});

/* auto increment js */
 /*$(document).ready(function(){
       function count($this){
           var current = parseInt($this.html(), 10);
           $this.html(++current);
           if(current !== $this.data('count')){
               setTimeout(function(){count($this)}, 10);
           }
       }        
       $(".team .col h6").each(function() {
           $(this).data('count', parseInt($(this).html(), 10));
           $(this).html('0');
           count($(this));
       });
   });*/

/* menu toggle */
$('.hamburger').click(function () {
      $('.hamburger').toggleClass('is-active'); 
      $('.mobile_toggle').toggleClass('is-active');   
}); 
/* mobile dropdown */
$('header nav>ul .drop').click(function () {
    if($('header nav>ul .drop .drop_box').hasClass('dr-active')) 
    {
        $('header nav>ul .drop .drop_box').removeClass('dr-active');    
        $(this).removeClass('is-active');
    }
    else{
        $(this).addClass('is-active');
        $(this).children().addClass('dr-active');
    }
});



