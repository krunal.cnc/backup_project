/* Window Load functions */

$(window).on('load',function(){
    setTimeout(function(){

    });
});


$(document).ready(function(){
    $(window).scroll(function(){
        if ($(window).scrollTop() >= 20) {
            $('header').addClass('sticky');
        }
        else {
            $('header').removeClass('sticky');
        }
    });
});

$(window).resize(function(){

})

equalheight = function(container){
    var currentTallest = 0,
        currentRowStart = 0,
        rowDivs = new Array(),
        $el,
        topPosition = 0;
    $(container).each(function() {

        $el = $(this);
        $($el).height('auto')
        topPostion = $el.position().top;

        if (currentRowStart != topPostion) {
            for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
            rowDivs.length = 0; // empty the array
            currentRowStart = topPostion;
            currentTallest = $el.height();
            rowDivs.push($el);
        } else {
            rowDivs.push($el);
            currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
        }
        for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
            rowDivs[currentDiv].height(currentTallest);
        }
    });
}

$(window).load(function() {
    equalheight('.why_total .why_box .box');
});


$(window).resize(function(){
    equalheight('.why_total .why_box .box');
});

/* on scroll smooth scroll */
$(document).ready(function () {
    $(document).on("scroll", onScroll);
    $('header .menu nav ul li a[href^="#"]').on('click', function (e) {
        e.preventDefault();
        $(document).off("scroll");
        $('header .menu nav ul li a').each(function () {
            $(this).removeClass('active');
        })
        $(this).addClass('active');
        var target = this.hash,
            menu = target;
        $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top - ($('header').height())
        }, 500, 'swing', function () {
            $(document).on("scroll", onScroll);
        });
    });


    $('#top_arrow').fadeOut('slow');
    $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('#top_arrow').fadeIn('slow');
        } else {
            $('#top_arrow').fadeOut('slow');
        }
    });
    $('#top_arrow').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 500);
        return false;
    });

});

function onScroll(event){
    var scrollPos = $(document).scrollTop();
    $('header .menu nav ul li a').each(function () {
        var currLink = $(this);
        var refElement = $(currLink.attr("href"));
        if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
            $('header .menu nav ul li a').removeClass("active");
            currLink.addClass("active");
        }
        else{
            currLink.removeClass("active");
        }
    });
}

Shadowbox.init({
    language: 'en',
    players: ['img', 'html', 'iframe', 'qt', 'wmp', 'swf', 'flv']
});


/* menu toggle */
jQuery('.hamburger').click(function () {
    jQuery('.hamburger').toggleClass('is-active'); 
    jQuery('header .right .menu').toggleClass('is-active');   
});


if ($(window).width() < 960) {
    $('.how_bg .how_work .step_section').owlCarousel({
        loop:true,                   
        items:2,
        rtl:true,
        center:true,
        autoWidth:true,
        smartSpeed:500,
        responsive:{
            0:{
                items:1
                
            },
            768:{
                items:2
            }
        }

    });
}
else {

}

$('header .menu nav ul li a').click(function(){
    jQuery('.hamburger').removeClass('is-active'); 
    jQuery('header .right .menu').removeClass('is-active');    
});


