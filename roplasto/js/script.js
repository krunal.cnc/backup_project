$(document).ready(function(){

    $('.door-section .col:last-of-type ul li a').click(function(){
        var _index = $(this).parent().index();

        $('.door-section .col:first-of-type img.default').fadeOut(300,function(){
            $(this).attr('src','images/site/big'+(_index+1)+'.jpg').fadeIn(200);
        });

        $('.door-section .col:first-of-type a.close').css('display','block');
    });

    $('.door-section .col:first-of-type a.close').click(function(){
        $('.door-section .col:first-of-type img.default').fadeOut(250,function(){
            $(this).attr('src','images/site/big22.jpg').fadeIn(150);
        });
        $(this).css('display','none');
    });
});



