/* Window Load functions */

$(window).on('load',function(){
    setTimeout(function(){
        $('.main .banner .banner_content').addClass('active');
    },100);
});


$(document).ready(function(){
    $(".main .banner .play").hover(
        function() {
            $('.right_circle').addClass("hover");
        }, function() {
            $('.right_circle').removeClass("hover");
        }
    );   

});


$(window).resize(function(){

})



$(".rslides").responsiveSlides({
    auto: true,             
    speed: 500,            
    timeout: 4000,          
    pager: true,           
    random: false, 
    pause: false,           
    pauseControls: true,    
    prevText: "Previous",   
    nextText: "Next",       
    namespace: "rslides",   
    before: function(){},   
    after: function(){}     
});


/* map */
function initMap() {
    var uluru = {lat: 50.930990, lng: 6.954290};
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15,                      
        center: uluru
    });
    var marker = new google.maps.Marker({
        position: uluru,
        map: map
    });
}


$(window).load(sticky_header);
$(window).scroll(sticky_header);
function sticky_header(){
    var scroll = $(window).scrollTop();    
    if (scroll > 10) {
        $("header").addClass("sticky");
        $(".main").addClass("sticky");
    }
    else {
        $("header").removeClass("sticky");
        $(".main").removeClass("sticky");
    }
}


/*  back to top */
$(".scroll_top").on("click",function() {
    $("html, body").animate({scrollTop: 0}, 1200);
});


/* pn scroll counter js */
var a = 0;
$(window).scroll(function() {
    var oTop = $('.number .section .box.line2').offset().top - window.innerHeight;
    if (a == 0 && $(window).scrollTop() > oTop) {
        $('.counter-value').each(function() {
            var $this = $(this),
                countTo = $this.attr('data-count');
            $({
                countNum: $this.text()
            }).animate({
                countNum: countTo
            },{
                duration: 2500,
                easing: 'swing',
                step: function() {
                    $this.text(Math.floor(this.countNum));
                },
                complete: function() {
                    $this.text(this.countNum);
                }

            });
        });
        a = 1;
    }

});


/* animation */
(function($) {
    $.fn.visible = function(partial) {
        var $t            = $(this),
            $w            = $(window),
            viewTop       = $w.scrollTop(),
            viewBottom    = viewTop + $w.height(),
            _top          = $t.offset().top,
            _bottom       = _top + $t.height(),
            compareTop    = partial === true ? _bottom : _top,
            compareBottom = partial === true ? _top : _bottom;

        return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
    };
})(jQuery);
var win = $(window);
var allMods = $(".dna_advantages .adv_wrap .list");
var allMods2 = $(".dna_advantages .adv_wrap .mob_middle");
var allMods3 = $(".testomonial .testo_text");
var allMods4 = $(".dna_partner .partners .img");

win.scroll(function(event) {
    allMods.each(function(i, el) {
        var el = $(el);
        if (el.visible(true)) {
            setTimeout(function(){
                el.addClass("come-in"); 
            },i*200);
        } 
    });
});

win.scroll(function(event) {
    allMods2.each(function(i, el) {
        var el = $(el);
        if (el.visible(true)) {
            setTimeout(function(){
                el.addClass("come-in"); 
            },i*200);
        } 
    });
});

win.scroll(function(event) {
    allMods3.each(function(i, el) {
        var el = $(el);
        if (el.visible(true)) {
            setTimeout(function(){
                el.addClass("come-in"); 
            },i*200);
        } 
    });
});

win.scroll(function(event) {
    allMods4.each(function(i, el) {
        var el = $(el);
        if (el.visible(true)) {
            setTimeout(function(){
                el.addClass("come-in"); 
            },i*100);
        } 
    });
});



/* on scroll smooth scroll */
$(document).ready(function () {
    $(document).on("scroll", onScroll);
    $('header .right .menu nav ul li a[href^="#"]').on('click', function (e) {
        e.preventDefault();
        $(document).off("scroll");

        $('a').each(function () {
            $(this).parents('li').removeClass('active');
        })
        $(this).parents('li').addClass('active');

        var target = this.hash,
            menu = target;
        $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top - (jQuery('header').outerHeight()) 
        }, 500, 'swing', function () {
            $(document).on("scroll", onScroll);
        });
    });
});

function onScroll(event){

    /*magic_line();*/

    var scrollPos = $(document).scrollTop();
    $('header .right .menu nav ul li a').each(function () {
        var count_rm = (jQuery('header').height()); //If Any additional height then minus it
        var currLink = $(this);
        var refElement = $(currLink.attr("href"));
        if ((refElement.position().top - count_rm) <= scrollPos && (refElement.position().top - count_rm) + refElement.height() > scrollPos) {
            $('header .right .menu nav ul li a').parents('li').removeClass("active");
            currLink.parents('li').addClass("active");
        }
        else{
            currLink.parents('li').removeClass("active");
        }
    });
}

/*
function magic_line(){
    var leftPos, newWidth, $magicLine;
    $magicLine = $('#magic-line');
    $magicLine.width($('header .right .menu nav ul li .active').width());
    $magicLine.css('left', $('.active a').position().left)
    $magicLine.data('origLeft', $magicLine.position().left)
    $magicLine.data('origWidth', $magicLine.width());
}
*/

/* magic line */
$(function() {
    'use strict';
    var leftPos, newWidth, $magicLine;

    $('.tabs').append("<li id='magic-line'><i></i></li>");
    $magicLine = $('#magic-line');
    $magicLine.width($('.active').width())
        .css('left', $('.active a').position().left)
        .data('origLeft', $magicLine.position().left)
        .data('origWidth', $magicLine.width());


    $('.tabs li a').click(function() {
        var $this = $(this);
        $this.parent().addClass('active').siblings().removeClass('active');
        $magicLine
            .data('origLeft', $this.position().left)
            .data('origWidth', $this.parent().width());
        return false;
    });

    /*Magicline hover animation*/
    $('.tabs li').find('a').hover(function() {
        var $thisBar = $(this);
        leftPos = $thisBar.position().left;
        newWidth = $thisBar.parent().width();
        $magicLine.css({
            "left": leftPos,
            "width": newWidth
        });
    }, function() {
        $magicLine.css({
            "left": $magicLine.data('origLeft'),
            "width": $magicLine.data('origWidth')
        });
    });
});