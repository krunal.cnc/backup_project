/* Window Load functions */

$(window).load(function(){
    $(window).scrollTop($(window).scrollTop() + 1);
});

$('#section1 .drop_box .top_col').click(function(){
    $(this).parent().toggleClass('active');
    $(this).toggleClass('active');
    $('#section1 .drop_box .drop_in').toggleClass('active');
    $('#section1 .drop_box .drop_in').slideToggle();

    if(!$('.page2 .drop_box .top_col').hasClass('active'))
    {
        $('.page2 .progress_card .col').removeClass('opcity_active');   
        $('.page2 .progress_card').removeClass('line');
    }
});


$('.page2 .drop_box .drop_in label').click(function(){
    if($(window).width() > 767){

    }else{        
        $('.page2 .drop_box .drop_in').removeClass('active');
        $('.page2 .drop_box .top_col').removeClass('active');
        $('.page2 .drop_box').removeClass('active');
        $('#section1 .drop_box .drop_in').slideUp();
    }
});

$('.page2 .progress_card .right_col').height($('.page2 .progress_card .left_col').height());
$('section').height($(window).height());
$('.banner').height($('section').height());
$(".banner .down_arrow a").click(function() {  
    if($(window).width() > 767){
        $('html, body').animate({

            scrollTop: $(".page2").offset().top + $('header').outerHeight()
        }, 500);  
    }else{
        $('html, body').animate({

            scrollTop: $(".page2").offset().top 
        }, 500);  
    }

});
$(document).ready(function() {



    $(window).scroll(function() {
        var sticky = $('header'),
            scroll = $(window).scrollTop();
        if (scroll > 50) sticky.addClass('fixed');
        else sticky.removeClass('fixed');
    });
});

$('#section1 .drop_box .drop_in label input[type="radio"]').change(function(){
    $('.page2 .progress_card').addClass('line');
    $('#section1 .drop_box .drop_in label').removeClass('active');
    $('.page2 .progress_card .col').removeClass('opcity_active');
    if(this.checked) {
        $(this).parent().addClass('active')
        var radio_check = $(this).val()
        $('.page2 .progress_card .col').each(function(){
            if(!$(this).hasClass(radio_check)){                
                $(this).addClass('opcity_active');
            }else{                

            }
        })
    }
    else{
        $(this).parent().removeClass('active')
    }
});

$(window).scroll(function() {
    /* var sticky1 = $('.page2'),*/
    var ban = $('.banner').outerHeight(),
        scroll1 = $(window).scrollTop();
    if (scroll1 < ban) {
        setTimeout(function(){
            $('.page2 .progress_card .col').each(function(i){
                var _m_ = $(this);
                setTimeout(function(){
                    _m_.addClass('come-in');
                },i*250)
            })    
        },250)
        setTimeout(function(){
            $('.page2 .progress_card .left_col').addClass('active');            
        },1000)


    }
});

$(".page2 .progress_card .col .bg_layer")
    .mouseover(function() {

    $('.page2 .progress_card .col').removeClass('active');
    $(this).parent('.col').removeClass('active');    

})
$(".page2 .progress_card .col .animate_block").mouseover(function() {
    $('.page2 .progress_card .col').removeClass('active');
    $(this).parent('.col').addClass('active');
});



$('.main .left').css('min-height',$(window).height());


/* menu toggle */
$('.hamburger').click(function () {
    $(this).toggleClass('active');
    $('nav').toggleClass('is-active');   
    $('.mob_col_menu').toggleClass('active');   
});