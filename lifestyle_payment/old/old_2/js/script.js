/* Window Load functions */

$(window).on('load',function(){
    //    $('.mid').fadeOut()
    setTimeout(function(){
        $('.loading-wrapper').width($('header').width())
        $('header').addClass('active')
        $('.section_header>*').each(function(i){
            var $this = $(this);
            setTimeout(function(){
                $this.addClass('active')
            },i*350)
        })
        $('.mid').fadeOut()
    },700);
    setTimeout(function(){
        $('.section_body').addClass('active');
        $('.card_page .box_section').addClass('active');
        $('.card_page .official').addClass('active');
        $('.card_page .new_card_offers').addClass('active');        
    },1500)
    setTimeout(function(){
        $('.loading-wrapper').addClass('active')
        $('header nav ul li').each(function(i){
            var $this = $(this);
            setTimeout(function(){
                $this.addClass('show')
            },i*350)
        })
    },1100)
    setTimeout(function(){
        $('.card_right_section .mid_card .card_big').addClass('active');
    },500)

    $('.fancy-select').parent().css('z-index',99999)
    var _x_ = $('.section_body .slides .body_part .col').length
    $('.section_body .slides .body_part .col').each(function(i){
        $(this).css('z-index',_x_ - i)
    })
});

$(document).ready(function(){
    $("form").attr('autocomplete', 'off');

    $('.checkbox input').change(function(){
        if(this.checked){
            $(this).parent().addClass('active')
        }
        else{
            $(this).parent().removeClass('active')
        }
    })

    $("input[type='email'],input[type='text'],input[type='tel'],input[type='number'],input[type='password'],input[type='date'],input[type='month']").change(function(){
        if($(this).val()){
            $(this).parent().addClass('has_val')
        }
        else{
            $(this).parent().removeClass('has_val')
        }
    });
});

$(window).resize(function(){

})

/* tab */
//$('.section_body .slides').click(function(){
//    $(this).find('.body_part').slideToggle();
//    $(this).toggleClass('active');
//});

$('.section_body .slides .body_part.digital .digital_ch .check_box .more_ingo_click').click(function(){
    $('.section_body .slides .body_part.digital .digital_ch .check_box .more_ingo').slideToggle();
    $(this).toggleClass('active');
});

/* checkbox */
$('.checkbox input').change(function(){
    if(this.checked){
        $(this).parent().addClass('active')
    }
    else{
        $(this).parent().removeClass('active')
    }
});

$('#datePicker').datepicker({
    dateFormat: 'mm-dd-yy',
    onSelect: function (selectedDate) {

    }

});
$('[data-toggle="datepicker2"]').datepicker({
    dateFormat: 'MM yy',
    onSelect: function (selectedDate) {

    }
});


//$('.section_body .slides .head h2').click(function(){
//    $(this).prev().trigger('click');
//})

/* mobile header append */
if ($(window).width() < 960) {
    $("header").after("<div class='mob_header'></div>");

    $("header nav ul li.active a .mob_show").each(function() {
        var a = $(this).html();
        $(".mob_header").append(a); 
    });
}
else {

}

$('label input[type=radio], label input[type=checkbox]').click(function() {
    $('label:has(input:checked)').addClass('active');
    $('label:has(input:not(:checked))').removeClass('active');
});


/* add on images on click  */
$(document).on('click','.section_body .slides .body_part.digital .digital_ch .check_box label',function(){
    //    $('.section_body .slides .body_part.digital .digital_ch .check_box .check_box_top').removeClass('show');
    if($(this).hasClass('active')){
        //        $('.section_body .slides .body_part.digital .digital_ch .check_box label').removeClass('active');
        $(this).parent().toggleClass('show');
    }
    else{}
});

/* popup */
$(window).load(function(){
    //save time pop
    //    $('.save_time_pop').addClass('active');

    //pop_back     
    //    $('.pop_back').addClass('active');

    //pop_exit    
    //    $('.pop_exit').addClass('active');

    //pop_fail    
    //    $('.pop_fail').addClass('active');

    //pop_up_back    
    //    $('.pop_up').addClass('active');    

    //pop_issuing
    setTimeout(function(){
        $('.pop_issuing').addClass('active');
    },800)
    $('.pop_issuing .issuing_pop_up .not_fill').click(function(){
        $('.pop_issuing').removeClass('active');    
    });

    /* on click return popup appears */
    $('.submit_btn .return').click(function(){
        $('.pop_back').addClass('active');
        $(this).addClass('pop_div');
    });

    /* pop_back cancelation */
    $('.pop_back .pop_back_content .btn_popup .cancelation').click(function(){
        $('.pop_back').removeClass('active');
    });


    $('.pop_back .pop_back_content .btn_popup .close').click(function(e){
        $('.pop_back').removeClass('active');
        if($('.section_body .slides.active').index() > 0){
            $('.section_body .slides.active .body_part').slideUp();
            $('.section_body .slides.active').removeClass('active').prev().addClass('active');
            $('.section_body .slides.active .body_part').slideDown();
        }
        else{
            $('.pop_back').removeClass('active');    
        }
    });

});
/* on input value checked */
$(function() {
    $("input[type='email'], input[type='text'], input[type='tel'], input[type='number'], input[type='password'], input[type='date'],input[type='month']").change(function() {
        var $input = $(this),
            $flag = $input.next();

        if (!$input.val()) {
            $flag.remove();
        }

        if ($flag.length == 0 || !$flag.is('.valid')) {
            $input.after('<div class="valid"><img src="./images/site/right_in.png" alt=""></div>');
            $('label b').css('display','none');
        }
    });
});

$('.section_body .slides .body_part .email a.re_mail').click(function(){
    $('.section_body .slides .body_part .bottom_detail').slideDown();
});


/* Academic degree */
$('.section_body .slides .body_part.client2 label.a_degree').click(function(){
    $('.section_body .slides .body_part .hidden_field').addClass('active');
});

/* Select the card you want */
$('.section_body .slides .body_part.card .select_card .card label').click(function(){
    $('.section_body .slides .body_part .ticket_cost').slideDown(); 
    $('.section_body .slides .body_part .billing_date').slideDown();
    $('.section_body .slides .body_part.card .select_card .card label').css('opacity','0.2')
    $(this).css('opacity','1');
});
$('.section_body .slides .body_part.digital .digital_ch .check_box .selects_ways_click').click(function(){
    $('.section_body .slides .body_part.digital .digital_ch .check_box .selects_ways').slideToggle();
    $(this).toggleClass('active');
});
/* Selects the ways to get information and actions */
$('.section_body .slides .body_part.digital .digital_ch .check_box .selects_ways a').click(function(){
    $(this).addClass('active');
});

$('.section_body.client3 .slides.active .body_part').slideDown();
/* spouse_id */
$('.section_body .slides.family_income .body_part .select_box.marital_status label[for="Status2"].married').click(function(){

    $('.section_body .slides .body_part label.spouse_id').addClass('active_div');
    //    $('.section_body .slides.family_income .body_part .select_box.marital_status label').removeClass('active');
    //    $(this).addClass('active');
});

/* monthly expenses */
$('.section_body .slides.family_income .body_part .select_box label.yes').click(function(event){
    $('.section_body .slides .body_part label.monthly_expenses').addClass('active_div');
    //    $('.section_body .slides.family_income .body_part .select_box label').removeClass('active');
    //    $(this).addClass('active');
});

/* laundering */ 
$('.section_body .slides.family_income .body_part .select_box label.yes_new').click(function(event){
    $('.section_body .slides .body_part label.refusal').addClass('active_div');
    //    $('.section_body .slides.family_income .body_part .select_box label').removeClass('active');
    //    $(this).addClass('active');

});


/* Continue click event */
$('.section_body .slides .body_part .continued').click(function(){
    var slide_length = $('.slides').length;
    var current_slide = $(this).parents('.slides').index();
    if(current_slide == (slide_length-1)){
        console.log('Last tab, No further')
    }else{
        $(this).parents('.body_part').slideUp();
        $(this).parents('.slides').removeClass('active');
        $(this).parents('.slides').next().addClass('active').find('.body_part').slideDown();
    }

});


/* disble from */
$(document).ready(function(){
    //Disable the anchor tag
    $(".continued").addClass('disabled');

});

$('input').change(function(){
    if(!$(this).val()){
        $(this).parent().addClass('error');
    }
    else{
        $(this).parent().removeClass('error');
        /*$(".continued").removeClass('disabled');*/
    }
});


/* from validation */
/* slides_client1_tab1*/
$(".section_body .slides_client1 .body_part label input").change(function(){
    var select1 = $('#select1').val();
    var spouse = $('#spouse').val();
    var name = $('#name').val();
    var lastname = $('#lastname').val();
    var id = $('#id').val();
    var phone = $('#phone').val();

    $('.section_body .slides.slides_client1 .body_part .bottom_detail label input[type="checkbox"]').change(function(){
        if(this.checked) {
            $(this).parent().addClass('active')
            $('#promotional').val('yes');
            if(select1 == "" ||  spouse == "" || name == "" || lastname == "" || id == "" || phone == "" || promotional == "")
            {
                $(".slides_client1 .continued").addClass('disabled');        
            }
            else{
                $(".slides_client1 .continued").removeClass('disabled');
            }
        }
        else{
            $(this).parent().removeClass('active')
            $('#promotional').val('No')
            $(".slides_client1 .continued").addClass('disabled');        
        }

    });

});

/* slides_client1_tab2 */
$(".section_body .slides_client2 .body_part label input").change(function(){
    var en_first = $('#en_first').val();
    var spouse_number = $('#spouse_number').val();
    var gender = $('input[name=gender]:checked').val();
    var city = $('#city').val();
    var street = $('#street').val();
    var nohome = $('#nohome').val();
    var apartment = $('#apartment').val();    
    var postal_code = $('#postal_code').val();    
    var mailbox = $('#mailbox').val();    
    var mail_adress = $('#mail_adress').val();    
    var datePicker = $('#datePicker').val();

    /*if(this.checked) {
        $(this).parent().addClass('active')
        $('#promotional2').val('yes');*/
    if(en_first == "" ||  spouse_number == "" || gender == "" || city == "" || street == "" || nohome == "" || apartment == "" || postal_code == "" || mailbox == "" || mail_adress == "" || datePicker == "")
    {
        $(".slides_client2 .continued").addClass('disabled');
    }
    else{
        $(".slides_client2 .continued").removeClass('disabled');
    }
    /*}
    else{
        $(this).parent().removeClass('active')
        $('#promotional2').val('No');
        $(".slide_client2_tab1 .continued").removeClass('disabled');
    }*/

});

/* slides_client2_tab1 */
$(".section_body .slide_client2_tab1 .body_part label input").change(function(){
    var account_owner = $('#account_owner').val();
    var account = $('input[name=account]:checked').val();
    var test2 = $('#test2').val();
    var branch_number = $('#branch_number').val();
    var friend_number = $('#friend_number').val();
    var acc_year = $('input[name=acc_year]:checked').val();
    var education = $('input[name=education]:checked').val();
    var test3 = $('#test3').val();
    if(account_owner == "" || account == null || test2 == "" || branch_number == "" || friend_number == "" || acc_year == null || education == null || test3 == "")
    {
        $(".slide_client2_tab1 .continued").addClass('disabled');
    }
    else{
        $(".slide_client2_tab1 .continued").removeClass('disabled');
    }

});

/* slides_client2_tab2 */
$(".section_body .slides_client2_tab2 .body_part label input").change(function(){
    var education = $('input[name=education]:checked').val();
    var per_month = $('input[name=per_month]:checked').val();
    if(education == "" || per_month == "")
    {
        $(".slides_client2_tab2 .continued").addClass('disabled');
    }
    else{
        $(".slides_client2_tab2 .continued").removeClass('disabled');
    }
});

/* slides_client2_tab3 */
$(document).ready(function(){
    $(".slides_client2_tab3 .continued").removeClass('disabled');
    $(".section_body.client5 .continued").removeClass('disabled');
    $(".slides_clent4_tab2 .continued").removeClass('disabled');
    //    $(".slide_client3_tab2 .continued").removeClass('disabled');
});

/* slides_client3_tab_1 */
$(".section_body .slide_client3_tab1 .body_part label input").change(function(){

    var test_expertise = $('#test_expertise').val();
    console.log(test_expertise);
    var role = $('#role').val();
    console.log(role);
    var test_job = $('#test_job').val();
    console.log(test_job);
    var acc_year = $('input[name=acc_year]:checked').val(); 
    console.log(acc_year);
    var test_public = $('#test_public').val(); 


    if(test_expertise == "" || role == "" || test_job == "" || acc_year == undefined || test_public == "")
    {
        $(".slide_client3_tab1 .continued").addClass('disabled');
    }
    else{
        $(".slide_client3_tab1 .continued").removeClass('disabled');
    }
});
/* slide_client3_tab2 */
$('.section_body .slides .body_part .select_box input').change(function(){

    var Status = $("input[name=Status]:checked").val();
    var acc2 = $("input[name=acc2]:checked").val(); 
    var acc3 = $("input[name=acc3]:checked").val(); 
    var laundering = $("input[name=laundering]:checked").val();
    var camp_1 = $(".irs-single").text();
    var laundering_new = $("input[name=laundering_new]:checked").val();

    if(Status == undefined || acc2 == undefined || acc3 == undefined || laundering == undefined || laundering_new == undefined)
    {
        $(".slide_client3_tab2 .continued").addClass('disabled');
    }
    else{
        $(".slide_client3_tab2 .continued").removeClass('disabled');
    }

});

/* slides_client4_tab1 */
//$(".section_body .slides_clent4_tab1 .body_part label input").change(function(){
$('.section_body .slides .body_part .submit_btn .right input[type="checkbox"]').bind("keyup change",function(e){
    var card_number = $('#card_number').val();
    var datePicker2 = $('#datePicker2').val();
    var promotional = $('#promotional').val();
    if(card_number == "" || datePicker2 == "")
    {
        $(".slides_clent4_tab1 .continued").addClass('disabled');
    }
    else{
        if(this.checked) {
            $(this).parent().addClass('active')
            $('#promotional').val('yes');
            $(".slides_clent4_tab1 .continued").removeClass('disabled');
        }
        else{
            $(this).parent().removeClass('active')
            $('#promotional').val('No')
            $(".slides_clent4_tab1 .continued").addClass('disabled');
        }
    }
});
/* new_card from valiadate */
$('.summary_email .left .submit').addClass('disble');
/* email valiadation */
$('.summary_email .left input').keyup(function(e){
    var sEmail = $('#email_id').val();


    if ($.trim(sEmail).length == 0) {
        e.preventDefault();
    }
    if (validateEmail(sEmail)) {
        $('.summary_email .left .submit').removeClass('disble');
        $('.summary_email .left label').removeClass('error_new');
    }
    else {
        $('.summary_email .left .submit').addClass('disble');
        $('.summary_email .left label').addClass('error_new');

        e.preventDefault();
    }
});
function validateEmail(sEmail) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
        return true;
    }
    else {
        return false;
    }
}
/* on click submit append value */
$(document).click(function(){
    $(".summary_email .left .submit").click(function() {
        var sEmail = $('#email_id').val();
        $('.card_page .box_section .summary_email p').text('סיכום הבקשה נשלח לכתובת מייל '+sEmail);
        $('.summary_email .right').find('img').attr('src','./images/site/mail_summary.png');
        $(this).parent().parent().parent().addClass('left_invisible');
    });    
});

/* on select specific option  input appear (A close associate of a public figure in the position)*/
function report(period) {
    if (period== "test_public3"){
        $('.section_body .slides .body_part label.public_role').addClass('show');
    }
    else if(period == "test_public4"){
        $('.section_body .slides .body_part label.public_role').addClass('show');
    }
    else if(period == "test_public5"){
        $('.section_body .slides .body_part label.public_role').addClass('show');
    }
    else{
        $('.section_body .slides .body_part label.public_role').removeClass('show');
    }
} 

/* flip */
$('.flip').click(function(){
    $(this).find('.card').toggleClass('flipped')
});

/* client2 page on click card  bottom text change */
$('.section_body .slides .body_part.card .select_card .card label').click(function(){
    $('.section_body .slides .body_part .ticket_cost h6').html($(this).find('h6').text());
    $('.section_body .slides .body_part .ticket_cost h4').html($(this).find('h4').text());
    $('.section_body .slides .body_part .ticket_cost p').html($(this).find('p').text()); 
});





