/* Window Load functions */

$(window).load(function(){
    setTimeout(function(){

    });
});

$(window).scroll(function() {
 
});
/* mobile menu */
  $('.container  .hamburger').click(function () {

      $('.container .hamburger').toggleClass('is-active'); 
      $('header .container .menu ').toggleClass('active');   
    
}); 
/* mobile menu dropdown */

     $('header .container .menu .drop_down').click(function () {
            if($('header .container .menu .drop_down .dropdown_menu').hasClass('dr-active')) 
            {
                $('header .container .menu .drop_down .dropdown_menu').removeClass('dr-active');    
                $(this).removeClass('is-active');
            }
            else{
                $('header .container .menu .drop_down .dropdown_menu').addClass('dr-active');
                $(this).addClass('is-active');
            }
    });


/* header sticky */
 $(document).ready(function(){
        $(window).scroll(function(){
        if ($(window).scrollTop()) {
            $('header').addClass('sticky');
        }
        else {
            $('header').removeClass('sticky');
        }
        });
    });

/* animated */
 $(function(){
        var $elems = $('.animateblock');
        var winheight = $(window).height();
        var fullheight = $(document).height();
        $(window).scroll(function(){
            animate_elems();
        });
        $(window).load(function(){
            animate_elems();
        })
        function animate_elems() {
            wintop = $(window).scrollTop(); // calculate distance from top of window

            // loop through each item to check when it animates
            $elems.each(function(){
                $elm = $(this);

                if($elm.hasClass('animated')) { return true; } // if already animated skip to the next item

                topcoords = $elm.offset().top; // element's distance from top of page in pixels

                if(wintop > (topcoords - (winheight*.75))) {
                    // animate when top of the window is 3/4 above the element
                    $elm.addClass('animated');
                }
            });
        }
    });


$(window).resize(function(){

})

 /* Mask Animation */
    $(function(){
        var $elems = $('.mask_blog,.animation_section');
        var winheight = $(window).height();
        var fullheight = $(document).height();
        $(window).scroll(function(){
            animate_elems();
        });
        $(window).load(function(){
            animate_elems();
        })
        function animate_elems() {
            wintop = $(window).scrollTop(); // calculate distance from top of window

            // loop through each item to check when it animates
            $elems.each(function(){
                $elm = $(this);

                if($elm.hasClass('animated')) { return true; } // if already animated skip to the next item

                topcoords = $elm.offset().top; // element's distance from top of page in pixels

                if(wintop > (topcoords - (winheight*.75))) {
                    // animate when top of the window is 3/4 above the element
                    $elm.addClass('animated');
                    $elm.children('.mask-anim').addClass('is-visible')
                }
            });
        }
    });

 /* on click of anchor smooth scroll offer sec */    
     $('.mouse-scroll a').click(function(e){
         e.preventDefault();
         var a = $(this).attr('href');

         $('html,body').animate({
           scrollTop: $(a).offset().top-110
         },800);    
    });   