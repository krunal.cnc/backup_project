<footer id="footer">
    <div class="footer-logo">
        <a href=""><img src="images/header-logo.png" alt=""></a>
    </div>
    <div class="footer-content clearfix">
        <div class="footer-bottom-nav">
            <ul>
                <li><a href="">News & Events</a></li>
                <li><a href="">Careers</a></li>
                <li><a href="">Customers</a></li>
                <li><a href="">Partners</a></li>
            </ul>
            <ul>
                <li><a href="">Investors</a></li>
                <li><a href="">Awards</a></li>
                <li><a href="">Services & Support</a></li>
                <li><a href="">Contact Us</a></li>
            </ul>
            <ul>
                <li><a href="">Product Overview</a></li>
                <li><a href="">Industry Solutions</a></li>
                <li><a href="">Technical Solutions</a></li>        
            </ul>
            <ul>
                <li><a href="">Documentation</a></li>
                <li><a href="">Blog</a></li>
                <li><a href="">Webinars</a></li>
                <li><a href="">Case Studies</a></li>
            </ul>
            <ul>
                <li><a href="">Benchmarks</a></li>
                <li><a href="">White Papers</a></li>
                <li><a href="">Brochures</a></li>
                <li><a href="">Presentations & Videos</a></li>
            </ul>
            <ul>
                <li><a href="">eBooks</a></li>
                <li><a href="">Glossary</a></li>
                <li><a href="">Site Map</a></li>
            </ul>
        </div>
        <div class="footer-social-copy-right">
            <div class="social">
                <a href="" target="_blank"><img src="images/social_1.png" alt=""></a>
                <a href="" target="_blank"><img src="images/social_2.png" alt=""></a>
                <a href="" target="_blank"><img src="images/social_3.png" alt=""></a>
                <a href="" target="_blank"><img src="images/social_4.png" alt=""></a>
            </div>
            <div class="cpoy-right">
                <p>All Rights Reserved to Ripples 2018©</p>
                <a href="">an <img src="images/copy_right.png" alt=""> site</a>
            </div>
        </div>
    </div>
</footer>
