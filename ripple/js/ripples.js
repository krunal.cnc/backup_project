/* Home page */
if(jQuery('.home').length > 0){

    setTimeout(function(){
        /*$grid=$('.grid')*/
        /*

        $grid.isotope({
            layoutMode: 'fitRows',
            itemSelector: '.grid-item',        
            fitRows: {
                gutter:10,
                columnWidth: 0,
            }
        });
*/
        /*$('.grid').isotope({
            itemSelector: '.grid-item',

            masonry: {
                fitWidth: true,            
                isOriginTop: true, 
                gutter:4,
                columnWidth: 20
            }
        });*/
        var msnry = new Masonry( '.masonry_col-gallery', {
            fitWidth: true,            
            isOriginTop: true,               
            columnWidth: 20,
            gutter:4
        });

    },350) 

    jQuery('.s3_carosel').owlCarousel({
        loop:true,
        nav:true,
        singleItem:true,
        items:1,
        slideSpeed : 300,
    });
    if (jQuery(window).width() < 1001) {
        jQuery('.s3_carosel1').owlCarousel({
            loop:true,
            nav:true,
            singleItem:true,
            items:4,
            slideSpeed : 300,
            responsiveClass:true,
            responsive:{
                0:{
                    items:2,
                },
                768:{
                    items:4,
                }
            }
        });

    }

    /* shadow box script */
    Shadowbox.init({
        language: 'en',
        players: ['img', 'html', 'iframe', 'qt', 'wmp', 'swf', 'flv']
    });


}

function section_height(){
    

    /* home banner */
    setTimeout(function(){
        $('.home #cover .bg').addClass('active');        
    },100)
    setTimeout(function(){
        $('#cover .col-left>*').addClass('active');        
    },250)
    setTimeout(function(){
        $('#cover .col-right').addClass('active');        
    },250)


    /* SAME HEIGHT BLOG */
    setTimeout(function(){
        $(".home .section-4 .section-4_content .s4_col .col-img").outerHeight("auto")
        var heights = $(".home .section-4 .section-4_content .s4_col .col-img").map(function (){
            return $(this).outerHeight();
        }).get(),maxHeight = Math.max.apply(null, heights);
        $(".home .section-4 .section-4_content .s4_col .col-img").outerHeight(maxHeight)
    },350) 



}


(function($) {
    $.fn.visible = function(partial) {
        var $t            = $(this),
            $w            = $(window),
            viewTop       = $w.scrollTop(),
            viewBottom    = viewTop + $w.height(),
            _top          = $t.offset().top,
            _bottom       = _top + $t.height(),
            compareTop    = partial === true ? _bottom : _top,
            compareBottom = partial === true ? _top : _bottom;

        return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
    };
})(jQuery);
var win = $(window);
var allMods = $(".home .section-1 .col");
var allMods1 = $(".home .section-4");
var allMods2 = $(".home .section-4 *");
var allMods3 = $(".home .section-5 h2");
var allMods4 = $(".home .section-5 .col-img");
var allMods5 = $(".home .section-5 .sectiob-5-form");
var allMods6 = $(".home .section-2 h2");
var allMods7 = $(".home .section-2 .section-2_content ul li");
var allMods8 = $(".home .section-3 .same");


win.scroll(function(event) {
    allMods.each(function(i, el) {
        var el = $(el);
        if (el.visible(true)) {
            setTimeout(function(){
                el.addClass("come-in"); 
            },i*200);
        } 
    });

    allMods1.each(function(i, el) {
        var el = $(el);
        if (el.visible(true)) {
            setTimeout(function(){
                el.addClass("come-in"); 
            },i*200);
        } 
    });


    allMods2.each(function(i, el) {
        var el = $(el);
        if (el.visible(true)) {
            setTimeout(function(){
                el.addClass("come-in"); 
            },i*50);
        } 
    });
    allMods3.each(function(i, el) {
        var el = $(el);
        if (el.visible(true)) {
            setTimeout(function(){
                el.addClass("come-in"); 
            },i*200);
        } 
    });
    allMods4.each(function(i, el) {
        var el = $(el);
        if (el.visible(true)) {
            setTimeout(function(){
                el.addClass("come-in"); 
            },i*200);
        } 
    });
    allMods5.each(function(i, el) {
        var el = $(el);
        if (el.visible(true)) {
            setTimeout(function(){
                el.addClass("come-in"); 
            },i*200);
        } 
    });
    allMods6.each(function(i, el) {
        var el = $(el);
        if (el.visible(true)) {
            setTimeout(function(){
                el.addClass("come-in"); 
            },i*200);
        } 
    });
    allMods7.each(function(i, el) {
        var el = $(el);
        if (el.visible(true)) {
            setTimeout(function(){
                el.addClass("come-in"); 
            },i*100);
        } 
    });
    allMods8.each(function(i, el) {
        var el = $(el);
        if (el.visible(true)) {
            setTimeout(function(){
                el.addClass("come-in"); 
            },i*200);
        } 
    });
});


/* mobile menu */
    jQuery(document).on('click', '.menu-trigger', function(){
        /*jQuery('.menu-trigger').click(function(){*/
        jQuery(this).toggleClass('active');
        jQuery('.ready').toggleClass('active');
        setTimeout(function(){
            jQuery('#header nav').toggleClass('active');
            jQuery('#header .menu-and-btns').toggleClass('active1');            
        },500)

    })
$(document).ready(section_height)
$(window).load(section_height);
$(window).resize(section_height);


