<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
        <link rel="stylesheet" href="font/fonts.css">
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <!--   <script src="https://isotope.metafizzy.co/isotope.pkgd.js"></script>-->
        <link rel="stylesheet" href="css/owl.carousel.min.css">
        <link rel="stylesheet" href="css/owl.theme.default.min.css">
        <link rel="stylesheet" href="css/shadowbox.css">
        <link rel="stylesheet" href="style.css">
        <title>Ripples | Home</title>
    </head>
    <body class="home">
        <?php require_once('header.php'); ?>
        <!--<div class="loader">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 150 150">
                <defs>
                    <linearGradient id="grad"  x1="0%" y1="50%" x2="0%" y2="30%" gradientUnits="userSpaceOnUse">
                        <stop offset="0" stop-color="#E3476F"/>
                        <stop offset="1" stop-opacity="0"  stop-color="#FFF"/>
                    </linearGradient>
                    <linearGradient id="refGrad"  x1="0%" y1="70" x2="0%" y2="90" gradientUnits="userSpaceOnUse">
                        <stop offset="0" stop-opacity="0.4" stop-color="#E3476F"/>
                        <stop offset="1" stop-opacity="0"  stop-color="#FFF"/>
                    </linearGradient>
                    <g id="whole">

                        <rect id="sqr" x="58" y="38" width="36" height="36" rx="0.74" ry="0.74"/>
                    </g>
                    <mask id="refGradMask">
                        <rect id="refSqr" x="58" y="74.1" width="36" height="36" rx="0.74" ry="0.74" fill="#FFF"/>
                    </mask>
                </defs>
                <g stroke="#FBE9DF" stroke-width="0.5" fill="none">
                    <ellipse class="rip" cx="110" cy="74" rx="0" ry="0" />
                    <ellipse class="rip" cx="110" cy="74" rx="0" ry="0" />
                    <ellipse class="rip" cx="110" cy="74" rx="0" ry="0" />
                </g>
                <use xlink:href="#whole" fill="#E3476F"/>
                <g mask="url(#refGradMask)">
                    <rect fill="url(#refGrad)" width="100%" height="50" x="0" y="73.4"/>
                </g>
                <g id="particleContainer"  fill="#E3476F">
                    <circle cx="110" cy="72" r="1" opacity="0"/>
                    <circle cx="110" cy="72" r="1" opacity="0"/>
                    <circle cx="110" cy="72" r="1" opacity="0"/>
                    <circle cx="110" cy="72" r="1" opacity="0"/>
                    <circle cx="110" cy="72" r="1" opacity="0"/>
                    <circle cx="110" cy="72" r="1" opacity="0"/>
                    <circle cx="110" cy="72" r="1" opacity="0"/>
                    <circle cx="110" cy="72" r="1" opacity="0"/>
                    <circle cx="110" cy="72" r="1" opacity="0"/>
                    <circle cx="110" cy="72" r="1" opacity="0"/>
                </g>
            </svg>
        </div>-->
        <div id="page-wrapper">
            <div id="cover">
                <div class="bg"></div>
                <!--<div class="site-width " data-stellar-offset-parent="true">-->
                <div class="site-width " >
                    <div class="col-left">
                        <h1 class="ripples-main-title">turn ordinary coffee into an extraordinary experience.</h1>
                        <p>With the Ripple Maker you’ll turn ordinary coffee into an extraordinary experience. Using patented printing technology, the machine creates inspiring Ripples from any image or text atop the foam layer of coffee beverages. Create a moment and forge a connection with your customers.</p>
                    </div>
                    <!--<div class="col-right" data-stellar-ratio="0.7">-->
                    <div class="col-right" >
                        <img src="images/banner_img1.png" alt="">
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div id="content" class="">
                <div class="section-1">
                    <div class="site-width">
                        <div class="section-1_content grid  ">
                            <div class="col grid-item grid_r1"><a href="https://www.youtube.com/embed/aVS4W7GZSq0" data-rel="shadowbox;width=1200;height=700;"><img src="images/s1_1.jpg" alt=""></a></div>
                            <div class="col grid-item grid_r1"><img src="images/s1_2.jpg" alt=""></div>
                            <div class="col grid-item grid_r3"><img src="images/s2.jpg" alt=""></div>
                            <div class="col grid-item grid_r2"><img src="images/s1_3.jpg" alt=""></div>
                            <div class="col grid-item grid_r2"><img src="images/s1_4.jpg" alt=""></div>

                        </div>
                    </div>
                </div>
                <div class="section-2">
                    <h2>Great ripple servers!</h2>
                    <div class="section-2_content">
                        <div class="site-width">
                            <ul class="s3_carosel1 cf">
                                <li class="cf" ><a href="javascript:void(0)"><img src="images/s2_1.png" alt=""></a></li>
                                <li class="cf" ><a href="javascript:void(0)"><img src="images/s2_2.png" alt=""></a></li>
                                <li class="cf" ><a href="javascript:void(0)"><img src="images/s2_3.png" alt=""></a></li>
                                <li class="cf" ><a href="javascript:void(0)"><img src="images/s2_1.png" alt=""></a></li>
                                <li class="cf" ><a href="javascript:void(0)"><img src="images/s2_2.png" alt=""></a></li>
                                <li class="cf" ><a href="javascript:void(0)"><img src="images/s2_3.png" alt=""></a></li>
                                <li class="cf" ><a href="javascript:void(0)"><img src="images/s2_1.png" alt=""></a></li>                                     
                                <li class="cf" ><a href="javascript:void(0)"><img src="images/s2_1.png" alt=""></a></li>
                                <li class="cf" ><a href="javascript:void(0)"><img src="images/s2_2.png" alt=""></a></li>
                                <li class="cf" ><a href="javascript:void(0)"><img src="images/s2_3.png" alt=""></a></li>
                                <li class="cf" ><a href="javascript:void(0)"><img src="images/s2_1.png" alt=""></a></li>
                                <li class="cf" ><a href="javascript:void(0)"><img src="images/s2_2.png" alt=""></a></li>
                                <li class="cf" ><a href="javascript:void(0)"><img src="images/s2_3.png" alt=""></a></li>
                                <li class="cf" ><a href="javascript:void(0)"><img src="images/s2_1.png" alt=""></a></li>                  
                            </ul>
                            <!-- <ul>
<li><a href="javascript:void(0)"><img src="images/s2_1.png" alt=""></a></li>
<li><a href="javascript:void(0)"><img src="images/s2_2.png" alt=""></a></li>
<li><a href="javascript:void(0)"><img src="images/s2_3.png" alt=""></a></li>
<li><a href="javascript:void(0)"><img src="images/s2_1.png" alt=""></a></li>
<li><a href="javascript:void(0)"><img src="images/s2_2.png" alt=""></a></li>
<li><a href="javascript:void(0)"><img src="images/s2_3.png" alt=""></a></li>
<li><a href="javascript:void(0)"><img src="images/s2_1.png" alt=""></a></li>        

<li><a href="javascript:void(0)"><img src="images/s2_1.png" alt=""></a></li>
<li><a href="javascript:void(0)"><img src="images/s2_2.png" alt=""></a></li>
<li><a href="javascript:void(0)"><img src="images/s2_3.png" alt=""></a></li>
<li><a href="javascript:void(0)"><img src="images/s2_1.png" alt=""></a></li>
<li><a href="javascript:void(0)"><img src="images/s2_2.png" alt=""></a></li>
<li><a href="javascript:void(0)"><img src="images/s2_3.png" alt=""></a></li>
<li><a href="javascript:void(0)"><img src="images/s2_1.png" alt=""></a></li>                  
</ul>-->
                        </div>
                    </div>
                </div>
                <div class="section-3 clearfix">
                    <div class="site-width">
                        <div class="col-left same">
                            <div class="s3_carosel">
                                <div class="s3_item">
                                    <span>25/04/18</span>
                                    <h3>Changing the way people drink</h3>
                                    <p>With the Ripple Maker you’ll turn ordinary coffee into an extraordinary experience. Using patented printing technology, the machine creates inspiring Ripples from any image or text.</p> 
                                    <a href="javascript:void(0)"  class="btn pink">Read more</a>
                                </div>
                                <div class="s3_item">
                                    <p>25/04/18</p>
                                    <h3>Changing the way people drink</h3>
                                    <p>With the Ripple Maker you’ll turn ordinary coffee into an extraordinary experience. Using patented printing technology, the machine creates inspiring Ripples from any image or text.</p>
                                    <a href="javascript:void(0)"  class="btn pink">Read more</a> 
                                </div>
                            </div>
                        </div>
                        <div class="col-right same" style="background-image:url(images/s3_img.jpg)"></div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="section-4">
                    <div class="site-width">
                        <h2>Give your drink the perferct finishing touch.</h2>
                        <div class="section-4_content">
                            <div class="s4_col">
                                <div class="col-img">
                                    <img src="images/s4_1.png" alt="">   
                                </div>
                                <a href="javascript:void(0)">coffee</a>
                                <p>check it out!</p>
                            </div>
                            <div class="s4_col">
                                <div class="col-img">
                                    <img src="images/s4_2.png" alt="">
                                </div>
                                <a href="javascript:void(0)">beer</a>
                                <p>check it out!</p>
                            </div>
                            <div class="s4_col">
                                <div class="col-img">
                                    <img src="images/s4_3.png" alt="">
                                </div>
                                <a href="javascript:void(0)">cocktails</a>
                                <p>check it out!</p>
                            </div>
                            <div class="s4_col">
                                <div class="col-img">
                                    <img src="images/s4_4.png" alt="">
                                </div>
                                <a href="javascript:void(0)">more!</a>
                                <p>check it out!</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section-5">
                    <div class="site-width">
                        <h2>ready to increase your brand awareness?</h2>
                        <div class="col-img">
                            <img src="images/s5_img.png" alt="">
                        </div>
                        <div class="sectiob-5-form">
                            <form action="">
                                <span>My name is</span><div class="input_border"><input type="text" placeholder="Johnny Cash"></div><span>, I work for</span><div class="input_border"><input type="text" placeholder="God himself" class="second-input"></div><br>           
                                <span>in lovely</span><div class="input_border"><input type="text" placeholder="Hollywood" class="third_input"></div><span>. My job is</span><div class="input_border"><input type="text" placeholder="Amazing" class="forth_input"></div><span>,</span>
                                <span>and I’m interested in</span><div class="input_border"><select name="" id=""><option value="Beer Ripples">Beer Ripples</option><option value="Beer Ripples">Beer Ripples</option></select></div><span>You can email me at</span>
                                <div class="input_border"><input type="email" placeholder="Johnny@Godhimself.com" class="last-input"></div>
                                <div class="form_submit">
                                    <input type="submit" value="send" class="btn pink"> 
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <?php require_once('footer.php'); ?>      
        </div><!--.page-wrapper-->
        <script src="js/owl.carousel.js"></script>
        <script src="js/shadowbox.js"></script>
        <script src="js/masonry.pkgd.js"></script>
        <script src="js/jquery.stellar.js"></script>
        <!-- <script src="js/masonry.js"></script>-->
        <script src="js/ripples.js"></script>
        <script>
            /*var msnry = new Masonry( '.masonry_col-gallery', {
                fitWidth: true,            
                isOriginTop: true,               
                columnWidth: 20,
                gutter:4
            });
*/
           /* $('.grid').masonry({
                // options
                itemSelector: '.grid-item',
                fitWidth: true,            
                isOriginTop: true, 
                gutter:4,
                columnWidth: 20
            });*/
             $('.grid').isotope({
                itemSelector: '.grid-item',

                masonry: {
                    fitWidth: true,            
                    isOriginTop: true, 
                    gutter:4,
                    columnWidth: 20
                }
            });

            $('.bg').stellar();
            $.stellar({
                horizontalScrolling: false
            });

            /*
             $.stellar({
                hideElement: function($elem) { $elem.css('opacity','0'); },
                showElement: function($elem) { $elem.css('opacity','1'); }
            });
*/
        </script>
        <script>
            var App = /** @class */ (function () {
                function App() {
                    var _this = this;
                    this.select = function (e) { return document.querySelector(e); };
                    this.selectAll = function (e) { return document.querySelectorAll(e); };
                    this.mainTl = new TimelineMax({ repeat: -1 });
                    this.grad = this.select('#grad');
                    this.particleArray = Array.from(this.selectAll('#particleContainer circle'));
                    this.playParticles = function () {
                        var d = _this.particleArray, i = d.length;
                        for (var _i = 0, d_1 = d; _i < d_1.length; _i++) {
                            var p = d_1[_i];
                            TweenMax.set(p, {
                                x: 0,
                                y: 0,
                                attr: {
                                    r: 1,
                                    cx: 110,
                                    cy: 75
                                },
                                alpha: 1,
                                scale: _this.randomBetween(5, 10) / 10,
                                transformOrigin: '50% 50%'
                            });
                            var tl = new TimelineMax();
                            tl.to(p, _this.randomBetween(2, 9) / 10, {
                                physics2D: {
                                    velocity: _this.randomBetween(24, 50),
                                    angle: _this.randomBetween(-120, -20),
                                    gravity: _this.randomBetween(55, 70)
                                },
                                scale: 0
                            });
                        }
                    };
                    var tl = new TimelineMax();
                    this.mainTl.add(tl);
                    TweenMax.set('#ref', {
                        scaleY: -1,
                        transformOrigin: 'center bottom'
                    });
                    tl.to('#sqr', 1, {
                        rotation: 90,
                        transformOrigin: '100% 100%',
                        ease: Expo.easeIn
                    })
                        .to('#refSqr', 1, {
                        rotation: -90,
                        transformOrigin: '100% 0%',
                        ease: Expo.easeIn
                    }, 0)
                        .from('#sqr', 1, {
                        scaleX: 0.9,
                        transformOrigin: '100% 100%',
                        immediateRender: false,
                        ease: Elastic.easeOut
                    }, 0.95)
                        .addCallback(this.doRipple, '-=0.95')
                        .addCallback(this.playParticles, '-=0.95')
                        .to(['#whole', '#refSqr'], 1.95, {
                        x: -36,
                        ease: Linear.easeNone
                    }, 0);
                }
                App.prototype.randomBetween = function (min, max) {
                    return Math.floor(Math.random() * (max - min + 1) + min);
                };
                App.prototype.doRipple = function () {
                    var tl = new TimelineMax();
                    tl.staggerFromTo('.rip', 2, {
                        attr: {
                            rx: 0,
                            ry: 0
                        },
                        alpha: 1
                    }, {
                        cycle: {
                            attr: [{ rx: 59, ry: 8 }, { rx: 39, ry: 5 }, { rx: 19, ry: 3 }]
                        },
                        alpha: 0,
                        ease: Circ.easeOut
                    }, 0.03);
                };
                Object.defineProperty(App.prototype, "timeline", {
                    get: function () {
                        return this.mainTl;
                    },
                    enumerable: true,
                    configurable: true
                });
                return App;
            }());
            TweenMax.set('svg', {
                visibility: 'visible'
            });
            var app = new App();


        </script>
    </body>
</html>