<?php
global $page_name; 
?>
<header id="header" class="clearfix <?php if( $page_name == 'search' ) echo 'init-sticky sticky'; ?>">
    <div class="logo">
        <a href="index.php"><img src="images/header-logo.png" alt="Lightbits"></a>
    </div>
    <div class="menu-trigger">
        <span class="line line-1"></span>
        <span class="line line-2"></span>
        <span class="line line-3"></span>
    </div>
    <!--<a href="javascript:void(0)" class="menu"><span></span></a>-->
    <div class="ready">
        <nav>
            <ul class="main-menu clearfix">
                <li><a href="#">Segments</a></li>
                <li><a href="#">Drink Types</a></li>
                <li><a href="#">Products</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Blog</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
        </nav>
        <div class="menu-and-btns clearfix">
            <div class="header-btns">
                <div class="header_logn_signup">
                    <a href="#">Log in / </a><a href="#">Signup</a>			        
                </div>
                <a href="#" class="btn pink">Contact us</a>
            </div>
        </div>
    </div>
    <!--.menu-and-btns-->
</header>