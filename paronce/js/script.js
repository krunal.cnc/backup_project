/* Window Load functions */

$(window).on('load',function(){
    setTimeout(function(){

    });
});


$(document).ready(function(){
    $('.banner').css('min-height',$(window).height());
});
$(window).resize(function(){

})

$(function() {
    'use strict';
    var leftPos, newWidth, $magicLine;

    $('.tabs').append("<li id='magic-line'><i></i></li>");
    $magicLine = $('#magic-line');
    $magicLine.width($('.active').width())
        .css('left', $('.active a').position().left)
        .data('origLeft', $magicLine.position().left)
        .data('origWidth', $magicLine.width());

    $('.tabs li a').click(function() {
        var $this = $(this);
        $this.parent().addClass('active').siblings().removeClass('active');
        $magicLine
            .data('origLeft', $this.position().left)
            .data('origWidth', $this.parent().width());
        return false;
    });

    /*Magicline hover animation*/
    $('.tabs li').find('a').hover(function() {
        var $thisBar = $(this);
        leftPos = $thisBar.position().left;
        newWidth = $thisBar.parent().width();
        $magicLine.css({
            "left": leftPos,
            "width": newWidth
        });
    }, function() {
        $magicLine.css({
            "left": $magicLine.data('origLeft'),
            "width": $magicLine.data('origWidth')
        });
    });
});

/* mobile menu */
$('.hamburger').click(function () {
    $(this).toggleClass('is-active'); 
    $('.right_menu').toggleClass('active');
});

$('.right_menu .menu nav ul li a').click(function(){
    $('.hamburger').removeClass('is-active');
    $('.right_menu').removeClass('active');
});


/*on scroll active change*/
$(document).ready(function () {

    $(document).on("scroll", onScroll);

    $('.right_menu .menu nav ul li a[href^="#"]').on('click', function (e) {
        e.preventDefault();
        $(document).off("scroll");

        $('.right_menu .menu nav ul li a').each(function () {
            $(this).removeClass('active');
        })
        $(this).addClass('active');

        var target = this.hash,
            menu = target;
        $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top /*- (($('header').height()))*/
        }, 500, 'swing', function () {

        });
    });
    function onScroll(event){
        var scrollPos = $(document).scrollTop();
        $('.right_menu .menu nav ul li a[href^="#"]').each(function () {
            var currLink = $(this);
            var refElement = $(currLink.attr("href"));
            if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
                $('.right_menu .menu nav ul li a').removeClass("active");
                currLink.addClass("active");
            }
            else{
                currLink.removeClass("active");
            }
        });
    }
}); 


/* animate */
$('.banner h1,h3').each(function(){
    $(this).html($(this).text().replace(/([^\x00-\x80]|\w)/g, "<span class='letter'>$&</span>"));
});

anime.timeline({loop:0}).add({
    targets: '.banner h1 .letter',
    translateX: [40,0],
    translateZ: 0,
    opacity: [0,1],
    easing: "easeOutExpo",
    duration: 2000,
    delay: function(el, i) {
        return 500 + 30 * i;
    }
});

(function($) {
    $.fn.visible = function(partial) {
        var $t            = $(this),
            $w            = $(window),
            viewTop       = $w.scrollTop(),
            viewBottom    = viewTop + $w.height(),
            _top          = $t.offset().top,
            _bottom       = _top + $t.height(),
            compareTop    = partial === true ? _bottom : _top,
            compareBottom = partial === true ? _top : _bottom;

        return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
    };
})(jQuery);
var win = $(window);
var allMods_1 = $("h3");

win.scroll(function(event) {

});





