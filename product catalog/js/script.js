/* Window Load functions */

$(window).load(function(){
    $('.grid-view .grid p').css('color','#192e96');
    jQuery('header').addClass('active');

  setTimeout(function(){
            
      $('.column-section .row .col').fadeIn() 
  
  }, 5000);
    
});

$(document).ready(function(){
    $('.column-section .row .col').css('display', 'none');
    $('.column-section .row .col').fadeIn(2000);
});
$(window).resize(function(){})

$(window).scroll(function(){
    var sticky = $('header'),
        scroll = $(window).scrollTop();

    if (scroll >= 50) sticky.addClass('fixed');
    else sticky.removeClass('fixed');
});

/*---nav li animation---------*/

$('header nav ul li a').click(function(){
      $('.hem_menu').toggleClass('active');
    $('nav').removeClass('active');
})

$('.column-section .pop').click(function(){
    $('.form-section').css({'display':'block','position':'absolute','z-index':'99999','height':'100vh','top':'115px','width':'100%'});
})
$('.form-section .close').click(function(){
    $('.form-section').css('display','none');
})

jQuery('.hem_menu').click(function(){
    $(this).toggleClass('active')
    $('header nav').toggleClass('active');
});

if($(window).width()<768){
    $('.product-catalog .product').css('background-image','../images/site/banner-img-small.jpg');
    $('.column-section .row .col').hide();
    $('.column-section .row .col:nth-of-type(-1n+8)').show();
}

if($(window).width()<490){
    $('.column-section .row .col').hide();
    $('.column-section .row .col:nth-of-type(-1n+4)').show();
}


$('.grid-view .list').click(function(){
    $('.column-section').addClass('active');
    $('.grid-view p').css('color','#9f9f9f');
    $(this).children('p').css('color','#192e96');
})

$('.grid-view .grid').click(function(){
    $('.column-section').removeClass('active');
    $('.grid-view p').css('color','#9f9f9f');
    $(this).children('p').css('color','#192e96');
})




//
//function sticky_relocate() {
//  var window_top = $(window).scrollTop();
//  var div_top = $('#sticky-anchor').offset().top;
//  if (window_top > div_top) {
//    $('#sticky').addClass('stick');
//  } else {
//    $('#sticky').removeClass('stick');
//  }
//}
//
//$(function() {
//  $(window).scroll(sticky_relocate);
//  sticky_relocate();
//});


$(window).scroll(function(){
    var sticky = $('.form-section'),
        scroll = $(window).scrollTop();

    if (scroll >= 250) sticky.addClass('stick');
    else sticky.removeClass('stick');
});

//
//$(function() {
//    var form = $(".form-section .wrapper");
//    $(window).scroll(function() {    
//        var scroll = $(window).scrollTop();
//    
//        if (scroll >= 150) {
//            form.css({'position':'fixed','z-index':'999','top':'255px','width':'100%','max-width':'100%'})
//        } else {
//            form.css({'position':'relative',})
//        }
//    });
//});