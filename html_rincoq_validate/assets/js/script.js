/* Window Load functions */

$(window).on('load',function(){
    setTimeout(function(){

    });
});




$(window).resize(function(){

})

/* tooltips */
$.fn.extend({
    popoverClosable: function (options) {
        var defaults = {
            template:
            '<div class="popover">\
<div class="arrow"></div>\
<div class="popover-header">\
<button type="button" class="close" data-dismiss="popover" aria-hidden="true">&times;</button>\
</div>\
<div class="popover-content"></div>\
</div>'
        };
        options = $.extend({}, defaults, options);
        var $popover_togglers = this;
        $popover_togglers.popover(options);
        $popover_togglers.on('click', function (e) {
            e.preventDefault();
            $popover_togglers.not(this).popover('hide');
        });
        $('html').on('click', '[data-dismiss="popover"]', function (e) {
            $popover_togglers.popover('hide');
        });
    }
});

$(function () {
    $('[data-toggle="popover"]').popoverClosable();
});
$('body').on('click', function (e) {
    $('[data-toggle="popover"]').each(function () {
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
            $('[data-toggle="popover"]').popoverClosable();
        }
    });
});


/* images change range slider */
var $rangeslider = $("#js-amount-range");
$rangeslider.rangeslider({
    polyfill: false
});   

var imageUrl = new Array();

imageUrl[0] = './assets/images/site/ra-joint-1@2x.png';
imageUrl[1] = './assets/images/site/ra-joint-2@2x.png';
imageUrl[2] = './assets/images/site/ra-joint-3@2x.png';
imageUrl[3] = './assets/images/site/ra-joint-4@2x.png';
imageUrl[4] = './assets/images/site/ra-joint-5@2x.png';
imageUrl[5] = './assets/images/site/ra-joint-6@2x.png';
imageUrl[6] = './assets/images/site/ra-joint-7@2x.png';

$(document).on('input change', '#js-amount-range', function() {
    var v = $(this).val();
    $("#joint_img").prop("src", imageUrl[v]);
});



/* close popup */
function closePopup(){
    $('.popup').removeClass('active');

    /* banner animation */
    $('.banner').addClass('animate');
}


/* header */
$('ul.nav li.dropdown').hover(function() {
    $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
}, function() {
    $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
});

$(document).ready(function() {                
    $('.watch_video').click(function(event){
        event.preventDefault();                
        $(".video_open").modal({show:true});
    });

});


if($(window).width()  > 1023 ){

    $('.navbar-nav>li.dropdown').each(function() {
        var currSelectValue = $(this).find('.dropdown-menu').stop(true, true).children('li');           
        var drw = $('.navbar-nav>li>.dropdown-menu>li').width() * currSelectValue.length ;
        $(this).find('.dropdown-menu').stop(true, true).width(drw + 60);
        $(this).find('.dropdown-menu').stop(true, true).css('left', - drw / 3);

    });

    $('ul.nav li.dropdown').hover(function() {
        $(this).find('.dropdown-menu').stop(true, true).addClass('dropopen');
    }, function() {
        $(this).find('.dropdown-menu').stop(true, true).removeClass('dropopen');
    });
}else{          

    $('.navbar-nav>li.dropdown>a').each(function() {
        var texta = $(this).text();
        $(this).next().prepend('<li><i>'+texta+'</i></li>' );                   
    });                
    $('.navbar-nav').prepend('<li class="close"></li>' );


    $(' .togglemenu').click(function(){
        $('.navbar').addClass('menuopen'); 
    });

    $(' .navbar-nav .close,.navbar-nav>li>a').click(function(){
        $('.navbar').removeClass('menuopen'); 
        $('.dropdown-menu').removeClass('subopen');
    });

    $('.navbar-nav>li.dropdown').prepend('<em></em>');


    $('.navbar-nav>li.dropdown em').click(function() {
        $(this).next().next().addClass('subopen');
    });
    $('.navbar-nav>li>.dropdown-menu>li i').click(function() {
        $('.dropdown-menu').removeClass('subopen');
    });
    $('.navbar-nav>li>.dropdown-menu>li>ul>li>a').click(function(){
        $('.dropdown-menu').removeClass('subopen');
        $('.navbar').removeClass('menuopen'); 
    })

}

/* ask us popup */
function ask_us_popup(){
    $('.popup.ask_us').addClass('active');
}
function leave_site(){
    $('.popup.leave_website').addClass('active');
}



/* on click smooth scroll */
$(".back_to_top").on('click', function(e) {
    e.preventDefault();
    var target = $(this).attr('href');
    $('html, body').animate({
        scrollTop: ($(target).offset().top)
    }, 1500);
});

/* ESC close popup */
$('body').keyup(function(event){
    if (event.keyCode == 27){
        console.log('ESC');
        $('.popup').removeClass('active');
        $('.form-signin [data-toggle="popover"]').popover('hide');
        /* banner animation */
        $('.banner').addClass('animate');
    }
});
/*$(document).keydown(function(e) {        
    e.preventDefault();
    if (e.keyCode == 27) {
        $('.popup').removeClass('active');
        $('.popover .popover-header .close').trigger();
    }
});*/


/* same height inner div */
var highestBox = 0;
$('.smart-words.range-value-current .tab-content>.tab-pane .inner-row-smoke').each(function(){  
    if($(this).height() > highestBox){  
        highestBox = $(this).height();  
    }
});    
$('.smart-words.range-value-current .tab-content>.tab-pane .inner-row-smoke').height(highestBox);


/* achievement */
$(document).ready(function() {
    var maxHeight = 0;
    $('.sholuder-section .tab-content>.tab-pane,.low-intensity .tab-content>.tab-pane').each(function() {
        maxHeight = $(this).height() > maxHeight ? $(this).height() : maxHeight;
    }).height(maxHeight);
    
    var maxHeight_new = 0;
    $('.regular_activity .tab-content>.tab-pane .inner-row-smoke').each(function() {
        maxHeight_new = $(this).outerHeight() > maxHeight_new ? $(this).outerHeight() : maxHeight_new;
    }).height(maxHeight_new);
    
    var maxHeight_new2 = 0;
    $('.smoking-values .tab-content>.tab-pane').each(function() {
        maxHeight_new2 = $(this).outerHeight() > maxHeight_new2 ? $(this).outerHeight() : maxHeight_new2;
    }).height(maxHeight_new2);
    
}); 








