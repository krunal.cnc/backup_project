/* Window Load functions */

$(window).on('load',function(){

    /* Scrolling */
    var winotp = $(window).scrollTop();
    (winotp > 50) ? $('header').addClass('sticky') : $('header').removeClass('sticky');
    (winotp > 50) ? $('#gotop').addClass('show') : $('#gotop').removeClass('show')

    setTimeout(function(){
        $('.loading').fadeOut()
    },1500)

    setTimeout(function(){
        $('.banner .banner_txt>*').each(function(i){
            var $this = $(this)
            setTimeout(function(){
                $this.addClass('animate')
            },i*150)
        })
        $('.contact_page .animate').each(function(i){
            var $this = $(this)
            setTimeout(function(){
                $this.addClass('come-in')
            },i*150)
        })
    },2000)


});


$(document).ready(function(){

    $('#gotop').click(function(){
        $('body, html').animate({scrollTop: 0});
    });


    (function($) {
        $.fn.visible = function(partial) {
            var $t            = $(this),
                $w            = $(window),
                viewTop       = $w.scrollTop(),
                viewBottom    = viewTop + $w.height(),
                _top          = $t.offset().top,
                _bottom       = _top + $t.height(),
                compareTop    = partial === true ? _bottom : _top,
                compareBottom = partial === true ? _top : _bottom;

            return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
        };
    })(jQuery);
    var win = $(window);
    var allMods = $(".project");
    var allMods_1 = $(".our_value .col_4,.contact_detail .col_4");
    var footer = $("footer .bottom_section a");
    var allMods_a1 = $(".history .history_inner .list_section,.history .history_inner .year");
    var allMods_a2 = $('.the_crew .col');
    var allMods_r1 = $('.banner  .col-lg-5 .banner_form>*');
    var allMods_r2 = $('.our_value_retail .col_4');
   
     win.scroll(function(event) {
        allMods_r2.each(function(i, el) {
            var el = $(el);
            if (el.visible(true)) {
                setTimeout(function(){
                    el.addClass("come-in"); 
                },i*200);
            } 
        });
    });
    win.scroll(function(event) {
        allMods_r1.each(function(i, el) {
            var el = $(el);
            if (el.visible(true)) {
                setTimeout(function(){
                    el.addClass("come-in"); 
                },i*200);
            } 
        });
    });
    win.scroll(function(event) {
        allMods.each(function(i, el) {
            var el = $(el);
            if (el.visible(true)) {
                setTimeout(function(){
                    el.addClass("come-in"); 
                },i*200);
            } 
        });
    });
    win.scroll(function(event) { 
        allMods_1.each(function(i, el) {
            var el = $(el);
            if (el.visible(true)) {
                if(!el.hasClass('come-in')){
                    setTimeout(function(){
                        el.addClass("come-in");
                    },i*150);
                }
            } 
        });
    });
    win.scroll(function(event) {
        footer.each(function(i, el) {
            var el = $(el);
            if (el.visible(true)) {
                if(!el.hasClass('come-in')){
                    setTimeout(function(){
                        el.addClass("come-in"); 
                    },i*150);
                }
            } 
        });
    });

    win.scroll(function(event) {
        allMods_a1.each(function(i, el) {
            var el = $(el);
            if (el.visible(true)) {
                setTimeout(function(){
                    el.addClass("come-in"); 
                },120);
            }
        });
    });
    
    win.scroll(function(event) {
        allMods_a2.each(function(i, el) {
            var el = $(el);
            if (el.visible(true)) {
                setTimeout(function(){
                    el.addClass("come-in"); 
                },i*200);
            }
        });
    });


    if($('#screen').length > 0){
        ! function(){
            "use strict";
            var lava0, lava1;
            var Point = function(x, y)
            {
                this.x = x;
                this.y = y;
                this.magnitude = x * x + y * y;
                this.computed = 0;
                this.force = 0;
            }
            Point.prototype.add = function(p)
            {
                return new Point(this.x + p.x, this.y + p.y);
            }
            var Ball = function(parent)
            {
                this.vel = new Point(
                    (Math.random() > 0.5 ? 1 : -1) * (0.2 + Math.random() * 0.25), (Math.random() > 0.5 ? 1 : -1) * (0.2 + Math.random() * 1)
                );
                this.pos = new Point(
                    parent.width * 0.2 + Math.random() * parent.width * 0.6,
                    parent.height * 0.2 + Math.random() * parent.height * 0.6
                );
                this.size = (parent.wh / 15) + Math.random() * (parent.wh / 15);
                this.width = parent.width;
                this.height = parent.height;
            }
            Ball.prototype.move = function()
            {
                if (this.pos.x >= this.width - this.size)
                {
                    if (this.vel.x > 0) this.vel.x = -this.vel.x;
                    this.pos.x = this.width - this.size;
                }
                else if (this.pos.x <= this.size)
                {
                    if (this.vel.x < 0) this.vel.x = -this.vel.x;
                    this.pos.x = this.size;
                }
                if (this.pos.y >= this.height - this.size)
                {
                    if (this.vel.y > 0) this.vel.y = -this.vel.y;
                    this.pos.y = this.height - this.size;
                }
                else if (this.pos.y <= this.size)
                {
                    if (this.vel.y < 0) this.vel.y = -this.vel.y;
                    this.pos.y = this.size;
                }

                this.pos = this.pos.add(this.vel);
            }
            var LavaLamp = function(width, height, numBalls, c0, c1)
            {
                this.step = 10;
                this.width = width;
                this.height = height;
                this.wh = Math.min(width, height);
                this.sx = Math.floor(this.width / this.step);
                this.sy = Math.floor(this.height / this.step);
                this.paint = false;
                this.metaFill = createRadialGradient(width, height, width, c0, c1);
                this.plx = [0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0];
                this.ply = [0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 0, 1];
                this.mscases = [0, 3, 0, 3, 1, 3, 0, 3, 2, 2, 0, 2, 1, 1, 0];
                this.ix = [1, 0, -1, 0, 0, 1, 0, -1, -1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1];
                this.grid = [];
                this.balls = [];
                this.iter = 0;
                this.sign = 1;
                for (var i = 0; i < (this.sx + 2) * (this.sy + 2); i++)
                {
                    this.grid[i] = new Point(
                        (i % (this.sx + 2)) * this.step, (Math.floor(i / (this.sx + 2))) * this.step
                    )
                }
                for (var i = 0; i < numBalls; i++)
                {
                    this.balls[i] = new Ball(this);
                }
            }
            LavaLamp.prototype.computeForce = function(x, y, idx)
            {
                var force;
                var id = idx || x + y * (this.sx + 2);
                if (x === 0 || y === 0 || x === this.sx || y === this.sy)
                {
                    var force = 0.6 * this.sign;
                }
                else
                {
                    var cell = this.grid[id];
                    var force = 0;
                    var i = 0,
                        ball;
                    while (ball = this.balls[i++])
                    {
                        force += ball.size * ball.size / (-2 * cell.x * ball.pos.x - 2 * cell.y * ball.pos.y + ball.pos.magnitude + cell.magnitude);
                    }
                    force *= this.sign
                }
                this.grid[id].force = force;
                return force;
            }

            LavaLamp.prototype.marchingSquares = function(next)
            {
                var x = next[0];
                var y = next[1];
                var pdir = next[2];
                var id = x + y * (this.sx + 2);
                if (this.grid[id].computed === this.iter) return false;
                var dir, mscase = 0;
                for (var i = 0; i < 4; i++)
                {
                    var idn = (x + this.ix[i + 12]) + (y + this.ix[i + 16]) * (this.sx + 2);
                    var force = this.grid[idn].force;
                    if ((force > 0 && this.sign < 0) || (force < 0 && this.sign > 0) || !force)
                    {

                        force = this.computeForce(
                            x + this.ix[i + 12],
                            y + this.ix[i + 16],
                            idn
                        );
                    }
                    if (Math.abs(force) > 1) mscase += Math.pow(2, i);
                }
                if (mscase === 15)
                {
                    return [x, y - 1, false];
                }
                else
                {
                    if (mscase === 5) dir = (pdir === 2) ? 3 : 1;
                    else if (mscase === 10) dir = (pdir === 3) ? 0 : 2;
                    else
                    {
                        dir = this.mscases[mscase];
                        this.grid[id].computed = this.iter;
                    }
                    var ix = this.step / (
                        Math.abs(Math.abs(this.grid[(x + this.plx[4 * dir + 2]) + (y + this.ply[4 * dir + 2]) * (this.sx + 2)].force) - 1) /
                        Math.abs(Math.abs(this.grid[(x + this.plx[4 * dir + 3]) + (y + this.ply[4 * dir + 3]) * (this.sx + 2)].force) - 1) + 1
                    );
                    ctx.lineTo(
                        this.grid[(x + this.plx[4 * dir + 0]) + (y + this.ply[4 * dir + 0]) * (this.sx + 2)].x + this.ix[dir] * ix,
                        this.grid[(x + this.plx[4 * dir + 1]) + (y + this.ply[4 * dir + 1]) * (this.sx + 2)].y + this.ix[dir + 4] * ix
                    );
                    this.paint = true;

                    return [
                        x + this.ix[dir + 4],
                        y + this.ix[dir + 8],
                        dir
                    ];
                }
            }
            LavaLamp.prototype.renderMetaballs = function()
            {
                var i = 0,
                    ball;
                while (ball = this.balls[i++]) ball.move();
                this.iter++;
                this.sign = -this.sign;
                this.paint = false;
                ctx.fillStyle = this.metaFill;
                ctx.beginPath();
                i = 0;
                ctx.shadowBlur = 50;
                ctx.shadowColor = "#f0f0f0";
                while (ball = this.balls[i++])
                {
                    var next = [
                        Math.round(ball.pos.x / this.step),
                        Math.round(ball.pos.y / this.step), false
                    ];
                    do {
                        next = this.marchingSquares(next);
                    } while (next);
                    if (this.paint)
                    {
                        ctx.fill();
                        ctx.closePath();
                        ctx.beginPath();
                        this.paint = false;
                    }
                }
            }
            var createRadialGradient = function(w, h, r, c0, c1)
            {
                var gradient = ctx.createRadialGradient(
                    w / 2, h / 2, 0,
                    w / 2, h / 2, r
                );
                gradient.addColorStop(0, c0);
                gradient.addColorStop(1, c1);
                return gradient;
            }
            var run = function()
            {
                requestAnimationFrame(run);
                ctx.clearRect(0, 0, screen.width, screen.height);
                lava0.renderMetaballs();
                lava1.renderMetaballs();
            }
            var screen = ge1doot.screen.init("screen", null, true),
                ctx = screen.ctx;
            screen.resize();
            lava0 = new LavaLamp(screen.width, screen.height, 10, "#fff", "#fff");
            lava1 = new LavaLamp(screen.width, screen.height, 10, "#fff", "#fff");
            run();
        }();    
    }



    /* Mobile Menu */
    if($('.mobile_menu').length > 0){
        ( function( window ) {'use strict';function classReg( className ) {return new RegExp("(^|\\s+)" + className + "(\\s+|$)");}var hasClass, addClass, removeClass;if ( 'classList' in document.documentElement ) {hasClass = function( elem, c ) {return elem.classList.contains( c );};addClass = function( elem, c ) {elem.classList.add( c );};removeClass = function( elem, c ) {elem.classList.remove( c );};}else {hasClass = function( elem, c ) {return classReg( c ).test( elem.className );};addClass = function( elem, c ) {if ( !hasClass( elem, c ) ) {elem.className = elem.className + ' ' + c;}};removeClass = function( elem, c ) {elem.className = elem.className.replace( classReg( c ), ' ' );};}function toggleClass( elem, c ) {var fn = hasClass( elem, c ) ? removeClass : addClass;fn( elem, c );}var classie = {hasClass: hasClass,addClass: addClass,removeClass: removeClass,toggleClass: toggleClass,has: hasClass,add: addClass,remove: removeClass,toggle: toggleClass};if ( typeof define === 'function' && define.amd ) {define( classie );} else {window.classie = classie;}})( window );
        (function() {var bodyEl = document.body,content = document.querySelector( '.content-wrap' ),openbtn = document.getElementById( 'open-button' ),closebtn = document.getElementById( 'close-button' ),isOpen = false,morphEl = document.getElementById( 'morph-shape' ),s = Snap( morphEl.querySelector( 'svg' ) );path = s.select( 'path' );initialPath = this.path.attr('d'),steps = morphEl.getAttribute( 'data-morph-open' ).split(';');stepsTotal = steps.length;isAnimating = false;function init() {initEvents();}function initEvents() {openbtn.addEventListener( 'click', toggleMenu );if( closebtn ) {closebtn.addEventListener( 'click', toggleMenu );}}function toggleMenu() {if( isAnimating ) return false;isAnimating = true;if( isOpen ) {classie.remove( bodyEl, 'show-menu' );setTimeout( function() {path.attr( 'd', initialPath );isAnimating = false; }, 300 );}else {classie.add( bodyEl, 'show-menu' );var pos = 0,nextStep = function( pos ) {if( pos > stepsTotal - 1 ) {isAnimating = false; return;}path.animate( { 'path' : steps[pos] }, pos === 0 ? 400 : 500, pos === 0 ? mina.easein : mina.elastic, function() { nextStep(pos); } );pos++;};nextStep(pos);}isOpen = !isOpen;}init();})();        
    }

    /* Scrolling */
    $(window).scroll(function(){
        var winotp = $(window).scrollTop();
        (winotp > 50) ? $('header').addClass('sticky') : $('header').removeClass('sticky');
        (winotp > 50) ? $('#gotop').addClass('show') : $('#gotop').removeClass('show')
    })


    /* Button  */
    'use strict';var _createClass = function () {function defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}return function (Constructor, protoProps, staticProps) {if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;};}();function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}var LiquidButton = function () {function LiquidButton(svg) {_classCallCheck(this, LiquidButton);var options = svg.dataset;this.id = this.constructor.id || (this.constructor.id = 1);this.constructor.id++;this.xmlns = 'http://www.w3.org/2000/svg';this.tension = options.tension * 1 || 0.4;this.width = options.width * 1 || 200;this.height = options.height * 1 || 50;this.margin = options.margin || 40;this.hoverFactor = options.hoverFactor || -0.1;this.gap = options.gap || 5;this.debug = options.debug || false;this.forceFactor = options.forceFactor || 0.2;this.color1 = options.color1 || '#36DFE7';this.color2 = options.color2 || '#8F17E1';this.color3 = options.color3 || '#BF09E6';this.textColor = options.textColor || '#FFFFFF';this.text = options.text || 'Button';this.svg = svg;this.layers = [{points: [],viscosity: 0.5,mouseForce: 100,forceLimit: 2}, {points: [],viscosity: 0.8,mouseForce: 150,forceLimit: 3}];for (var layerIndex = 0; layerIndex < this.layers.length; layerIndex++) {var layer = this.layers[layerIndex];layer.viscosity = options['layer-' + (layerIndex + 1) + 'Viscosity'] * 1 || layer.viscosity;layer.mouseForce = options['layer-' + (layerIndex + 1) + 'MouseForce'] * 1 || layer.mouseForce;layer.forceLimit = options['layer-' + (layerIndex + 1) + 'ForceLimit'] * 1 || layer.forceLimit;layer.path = document.createElementNS(this.xmlns, 'path');this.svg.appendChild(layer.path);}this.wrapperElement = options.wrapperElement || document.body;if (!this.svg.parentElement) {this.wrapperElement.append(this.svg);}this.svgText = document.createElementNS(this.xmlns, 'text');this.svgText.setAttribute('x', '50%');this.svgText.setAttribute('y', '51%');this.svgText.setAttribute('dy', ~~(this.height / 8) + 'px');this.svgText.setAttribute('font-size', 14);this.svgText.style.fontFamily = 'Muli';this.svgText.setAttribute('text-anchor', 'middle');this.svgText.setAttribute('pointer-events', 'none');this.svg.appendChild(this.svgText);this.svgDefs = document.createElementNS(this.xmlns, 'defs');this.svg.appendChild(this.svgDefs);this.touches = [];this.noise = options.noise || 0;document.body.addEventListener('touchstart', this.touchHandler);document.body.addEventListener('touchmove', this.touchHandler);document.body.addEventListener('touchend', this.clearHandler);document.body.addEventListener('touchcancel', this.clearHandler);this.svg.addEventListener('mousemove', this.mouseHandler);this.svg.addEventListener('mouseout', this.clearHandler);this.initOrigins();this.animate();}LiquidButton.prototype.distance = function distance(p1, p2) {return Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2));};LiquidButton.prototype.update = function update() {for (var layerIndex = 0; layerIndex < this.layers.length; layerIndex++) {var layer = this.layers[layerIndex];var points = layer.points;for (var pointIndex = 0; pointIndex < points.length; pointIndex++) {var point = points[pointIndex];var dx = point.ox - point.x + (Math.random() - 0.5) * this.noise;var dy = point.oy - point.y + (Math.random() - 0.5) * this.noise;var d = Math.sqrt(dx * dx + dy * dy);var f = d * this.forceFactor;point.vx += f * (dx / d || 0);point.vy += f * (dy / d || 0);for (var touchIndex = 0; touchIndex < this.touches.length; touchIndex++) {var touch = this.touches[touchIndex];var mouseForce = layer.mouseForce;if (touch.x > this.margin && touch.x < this.margin + this.width && touch.y > this.margin && touch.y < this.margin + this.height) {mouseForce *= -this.hoverFactor;}var mx = point.x - touch.x;var my = point.y - touch.y;var md = Math.sqrt(mx * mx + my * my);var mf = Math.max(-layer.forceLimit, Math.min(layer.forceLimit, mouseForce * touch.force / md));point.vx += mf * (mx / md || 0);point.vy += mf * (my / md || 0);}point.vx *= layer.viscosity;point.vy *= layer.viscosity;point.x += point.vx;point.y += point.vy;}for (var pointIndex = 0; pointIndex < points.length; pointIndex++) {var prev = points[(pointIndex + points.length - 1) % points.length];var point = points[pointIndex];var next = points[(pointIndex + points.length + 1) % points.length];var dPrev = this.distance(point, prev);var dNext = this.distance(point, next);var line = {x: next.x - prev.x,y: next.y - prev.y};var dLine = Math.sqrt(line.x * line.x + line.y * line.y);point.cPrev = {x: point.x - line.x / dLine * dPrev * this.tension,y: point.y - line.y / dLine * dPrev * this.tension};point.cNext = {x: point.x + line.x / dLine * dNext * this.tension,y: point.y + line.y / dLine * dNext * this.tension};}}};LiquidButton.prototype.animate = function animate() {var _this = this;this.raf(function () {_this.update();_this.draw();_this.animate();});};LiquidButton.prototype.draw = function draw() {for (var layerIndex = 0; layerIndex < this.layers.length; layerIndex++) {var layer = this.layers[layerIndex];if (layerIndex === 1) {if (this.touches.length > 0) {while (this.svgDefs.firstChild) {this.svgDefs.removeChild(this.svgDefs.firstChild);}for (var touchIndex = 0; touchIndex < this.touches.length; touchIndex++) {var touch = this.touches[touchIndex];var gradient = document.createElementNS(this.xmlns, 'radialGradient');gradient.id = 'liquid-gradient-' + this.id + '-' + touchIndex;var start = document.createElementNS(this.xmlns, 'stop');start.setAttribute('stop-color', this.color3);start.setAttribute('offset', '0%');var stop = document.createElementNS(this.xmlns, 'stop');stop.setAttribute('stop-color', this.color2);stop.setAttribute('offset', '100%');gradient.appendChild(start);gradient.appendChild(stop);this.svgDefs.appendChild(gradient);gradient.setAttribute('cx', touch.x / this.svgWidth);gradient.setAttribute('cy', touch.y / this.svgHeight);gradient.setAttribute('r', touch.force);layer.path.style.fill = 'url(#' + gradient.id + ')';}} else {layer.path.style.fill = this.color2;}} else {layer.path.style.fill = this.color1;}var points = layer.points;var commands = [];commands.push('M', points[0].x, points[0].y);for (var pointIndex = 1; pointIndex < points.length; pointIndex += 1) {commands.push('C', points[(pointIndex + 0) % points.length].cNext.x, points[(pointIndex + 0) % points.length].cNext.y, points[(pointIndex + 1) % points.length].cPrev.x, points[(pointIndex + 1) % points.length].cPrev.y, points[(pointIndex + 1) % points.length].x, points[(pointIndex + 1) % points.length].y);}commands.push('Z');layer.path.setAttribute('d', commands.join(' '));}this.svgText.textContent = this.text;this.svgText.style.fill = this.textColor;};LiquidButton.prototype.createPoint = function createPoint(x, y) {return {x: x,y: y,ox: x,oy: y,vx: 0,vy: 0};};LiquidButton.prototype.initOrigins = function initOrigins() {this.svg.setAttribute('width', this.svgWidth);this.svg.setAttribute('height', this.svgHeight);for (var layerIndex = 0; layerIndex < this.layers.length; layerIndex++) {var layer = this.layers[layerIndex];var points = [];for (var x = ~~(this.height / 2); x < this.width - ~~(this.height / 2); x += this.gap) {points.push(this.createPoint(x + this.margin, this.margin));}for (var alpha = ~~(this.height * 1.25); alpha >= 0; alpha -= this.gap) {var angle = Math.PI / ~~(this.height * 1.25) * alpha;points.push(this.createPoint(Math.sin(angle) * this.height / 2 + this.margin + this.width - this.height / 2, Math.cos(angle) * this.height / 2 + this.margin + this.height / 2));}for (var x = this.width - ~~(this.height / 2) - 1; x >= ~~(this.height / 2); x -= this.gap) {points.push(this.createPoint(x + this.margin, this.margin + this.height));}for (var alpha = 0; alpha <= ~~(this.height * 1.25); alpha += this.gap) {var angle = Math.PI / ~~(this.height * 1.25) * alpha;points.push(this.createPoint(this.height - Math.sin(angle) * this.height / 2 + this.margin - this.height / 2, Math.cos(angle) * this.height / 2 + this.margin + this.height / 2));}layer.points = points;}};_createClass(LiquidButton, [{key: 'mouseHandler',get: function get() {var _this2 = this;return function (e) {_this2.touches = [{x: e.offsetX,y: e.offsetY,force: 1}];};}}, {key: 'touchHandler',get: function get() {var _this3 = this;return function (e) {_this3.touches = [];var rect = _this3.svg.getBoundingClientRect();for (var touchIndex = 0; touchIndex < e.changedTouches.length; touchIndex++) {var touch = e.changedTouches[touchIndex];var x = touch.pageX - rect.left;var y = touch.pageY - rect.top;if (x > 0 && y > 0 && x < _this3.svgWidth && y < _this3.svgHeight) {_this3.touches.push({x: x,y: y,force: touch.force || 1});}}/*e.preventDefault();*/};}}, {key: 'clearHandler',get: function get() {var _this4 = this;return function (e) {_this4.touches = [];};}}, {key: 'raf',get: function get() {return this.__raf || (this.__raf = (window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function (callback) {setTimeout(callback, 10);}).bind(window));}}, {key: 'svgWidth',get: function get() {return this.width + this.margin * 2;}}, {key: 'svgHeight',get: function get() {return this.height + this.margin * 2;}}]);return LiquidButton;}();var redraw = function redraw() {button.initOrigins();};var buttons = document.getElementsByClassName('liquid-button');for (var buttonIndex = 0; buttonIndex < buttons.length; buttonIndex++) {var _button = buttons[buttonIndex];_button.liquidButton = new LiquidButton(_button);}


    /* Slider */
    var swiper = new Swiper('.banner .swiper-container', {
        spaceBetween: 30,
        loop:false,
        effect: 'fade',
        speed: 500,
        centeredSlides: true,
        autoplay: {
            delay: 2500,
            disableOnInteraction: false,
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });

    /* Bubble Effect */
    var el1 = $('#el1'),
        el2 = $('#el2'),
        circle = $('#circle');


    var el3 = $('#el3'),
        el4 = $('#el4'),
        circle1 = $('#circle1');
    var t = new TimelineMax({repeat: -1});
    TweenMax.to(el1, 10, {
        x: '+120', 
        y: '+100',
        repeat:-1,
        yoyo:true,
        ease:Linear.easeNone
    });
    TweenMax.to(el2, 9, {
        x: '-140', 
        y: '+100',
        repeatDelay: 1,
        repeat:-1,
        yoyo:true,
        ease:Linear.easeNone
    });

    TweenMax.to(el3, 15, {
        x: '+120', 
        y: '+100',
        repeat:-1,
        yoyo:true,
        ease:Linear.easeNone
    });
    TweenMax.to(el4, 14, {
        x: '-120', 
        y: '+100',
        repeatDelay: 3,
        repeat:-1,
        yoyo:true,
        ease:Linear.easeNone
    });

    if($(window).width() > 767 && $('.btn-liquid').length > 0){
        $(function() {var pointsA = [],pointsB = [],$canvas = null,canvas = null,context = null,vars = null,points = 8,viscosity = 100,mouseDist = 150,damping = 0.08,showIndicators = false;mouseX = 0,mouseY = 0,relMouseX = 0,relMouseY = 0,mouseLastX = 0,mouseLastY = 0,mouseDirectionX = 0,mouseDirectionY = 0,mouseSpeedX = 0,mouseSpeedY = 0;function mouseDirection(e) {if (mouseX < e.pageX)mouseDirectionX = 1;else if (mouseX > e.pageX)mouseDirectionX = -1;elsemouseDirectionX = 0;if (mouseY < e.pageY)mouseDirectionY = 1;else if (mouseY > e.pageY)mouseDirectionY = -1;elsemouseDirectionY = 0;mouseX = e.pageX;mouseY = e.pageY;relMouseX = (mouseX - $canvas.offset().left);relMouseY = (mouseY - $canvas.offset().top);}$(document).on('mousemove', mouseDirection);function mouseSpeed() {mouseSpeedX = mouseX - mouseLastX;mouseSpeedY = mouseY - mouseLastY;mouseLastX = mouseX;mouseLastY = mouseY;setTimeout(mouseSpeed, 50);}mouseSpeed();function initButton() {var button = $('.btn-liquid');var buttonWidth = button.width();var buttonHeight = button.height();$canvas = $('<canvas id="button_liq"></canvas>');button.append($canvas);canvas = $canvas.get(0);canvas.width = buttonWidth+100;canvas.height = buttonHeight+100;context = canvas.getContext('2d');var x = buttonHeight/2;for(var j = 1; j < points; j++) {addPoints((x+((buttonWidth-buttonHeight)/points)*j), 0);}addPoints(buttonWidth-buttonHeight/5, 0);addPoints(buttonWidth+buttonHeight/10, buttonHeight/2);addPoints(buttonWidth-buttonHeight/5, buttonHeight);for(var j = points-1; j > 0; j--) {addPoints((x+((buttonWidth-buttonHeight)/points)*j), buttonHeight);}addPoints(buttonHeight/5, buttonHeight);addPoints(-buttonHeight/10, buttonHeight/2);addPoints(buttonHeight/5, 0);renderCanvas();}function addPoints(x, y) {pointsA.push(new Point(x, y, 1));pointsB.push(new Point(x, y, 2));}function Point(x, y, level) {this.x = this.ix = 50+x;this.y = this.iy = 50+y;this.vx = 0;this.vy = 0;this.cx1 = 0;this.cy1 = 0;this.cx2 = 0;this.cy2 = 0;this.level = level;}Point.prototype.move = function() {this.vx += (this.ix - this.x) / (viscosity*this.level);this.vy += (this.iy - this.y) / (viscosity*this.level);var dx = this.ix - relMouseX,dy = this.iy - relMouseY;var relDist = (1-Math.sqrt((dx * dx) + (dy * dy))/mouseDist);if ((mouseDirectionX > 0 && relMouseX > this.x) || (mouseDirectionX < 0 && relMouseX < this.x)) {if (relDist > 0 && relDist < 1) {this.vx = (mouseSpeedX / 4) * relDist;}}this.vx *= (1 - damping);this.x += this.vx;if ((mouseDirectionY > 0 && relMouseY > this.y) || (mouseDirectionY < 0 && relMouseY < this.y)) {if (relDist > 0 && relDist < 1) {this.vy = (mouseSpeedY / 4) * relDist;}}this.vy *= (1 - damping);this.y += this.vy;};function renderCanvas() {rafID = requestAnimationFrame(renderCanvas);context.clearRect(0, 0, $canvas.width(), $canvas.height());context.fillStyle = '#fff';context.fillRect(0, 0, $canvas.width(), $canvas.height());for (var i = 0; i <= pointsA.length - 1; i++) {pointsA[i].move();pointsB[i].move();}var gradientX = Math.min(Math.max(mouseX - $canvas.offset().left, 0), $canvas.width());var gradientY = Math.min(Math.max(mouseY - $canvas.offset().top, 0), $canvas.height());var distance = Math.sqrt(Math.pow(gradientX - $canvas.width()/2, 2) + Math.pow(gradientY - $canvas.height()/2, 2)) / Math.sqrt(Math.pow($canvas.width()/2, 2) + Math.pow($canvas.height()/2, 2));var gradient = context.createRadialGradient(gradientX, gradientY, 300+(300*distance), gradientX, gradientY, 0);gradient.addColorStop(0, '#ffa688');gradient.addColorStop(1, '#ff6176');var groups = [pointsA, pointsB];for (var j = 0; j <= 1; j++) {var points = groups[j];if (j == 0) {context.fillStyle = '#943dff';} else {context.fillStyle = gradient;}context.beginPath();context.moveTo(points[0].x, points[0].y);for (var i = 0; i < points.length; i++) {var p = points[i];var nextP = points[i + 1];var val = 30*0.552284749831;if (nextP != undefined) {p.cx1 = (p.x+nextP.x)/2;p.cy1 = (p.y+nextP.y)/2;p.cx2 = (p.x+nextP.x)/2;p.cy2 = (p.y+nextP.y)/2;context.bezierCurveTo(p.x, p.y, p.cx1, p.cy1, p.cx1, p.cy1);} else {nextP = points[0];p.cx1 = (p.x+nextP.x)/2;p.cy1 = (p.y+nextP.y)/2;context.bezierCurveTo(p.x, p.y, p.cx1, p.cy1, p.cx1, p.cy1);}}context.fill();}if (showIndicators) {context.fillStyle = '#000';context.beginPath();for (var i = 0; i < pointsA.length; i++) {var p = pointsA[i];context.rect(p.x - 1, p.y - 1, 2, 2);}context.fill();context.fillStyle = '#f00';context.beginPath();for (var i = 0; i < pointsA.length; i++) {var p = pointsA[i];context.rect(p.cx1 - 1, p.cy1 - 1, 2, 2);context.rect(p.cx2 - 1, p.cy2 - 1, 2, 2);}context.fill();}}initButton();});
    }



});

$(window).resize(function(){

})
