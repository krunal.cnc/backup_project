/* Window Load functions */

$(window).on('load',function(){
    setTimeout(function(){
        $(".instagram_school").addClass("load_done");    
    },500);
});


$(document).ready(function(){

    (function($) {
        $.fn.visible = function(partial) {
            var $t            = $(this),
                $w            = $(window),
                viewTop       = $w.scrollTop(),
                viewBottom    = viewTop + $w.height(),
                _top          = $t.offset().top,
                _bottom       = _top + $t.height(),
                compareTop    = partial === true ? _bottom : _top,
                compareBottom = partial === true ? _top : _bottom;

            return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
        };
    })(jQuery);
    var win = $(window);
    /*var allMods = $(".instagram_school");*/
    var allMods2 = $(".full_detail .right");
    var allMods3 = $(".full_detail .left");
    var allMods4 = $(".water_images .Syllabus");

    /*win.scroll(function(event) {
        allMods.each(function(i, el) {
            var el = $(el);
            if (el.visible(true)) {
                setTimeout(function(){
                    el.addClass("load_done"); 
                },i*200);
            } 
        });
    });*/

    win.scroll(function(event) {
        allMods2.each(function(i, el) {
            var el = $(el);
            if (el.visible(true)) {
                setTimeout(function(){
                    el.addClass("load_done"); 
                },i*200);
            } 
        });
    });

    win.scroll(function(event) {
        allMods3.each(function(i, el) {
            var el = $(el);
            if (el.visible(true)) {
                setTimeout(function(){
                    el.addClass("load_done"); 
                },i*200);
            } 
        });
    });

    win.scroll(function(event) {
        allMods4.each(function(i, el) {
            var el = $(el);
            if (el.visible(true)) {
                setTimeout(function(){
                    el.addClass("load_done"); 
                },i*200);
            } 
        });
    });


});

$(window).resize(function(){

})

TweenMax.set(blueFish1, {x: '+=400'});
TweenMax.set(blueFish2, {x: '+=500'});
TweenMax.set(blueFish3, {x: '+=900'});
TweenMax.set(blueFish4, {x: '+=600'});

TweenMax.set(bfish1, {x: '+=600'});
TweenMax.set(bfish2, {x: '+=600'});
TweenMax.set(bfish3, {x: '+=600'});
TweenMax.set(bfish4, {x: '+=600'});
TweenMax.set(bfish5, {x: '+=600'});
TweenMax.set(bfish6, {x: '+=600'});
TweenMax.set(bfish7, {x: '+=600'});
TweenMax.set(bfish8, {x: '+=600'});
TweenMax.set(bfish9, {x: '+=600'});

TweenMax.set(d_fish1, {x: '+=500'});
TweenMax.set(d_fish2, {x: '+=500'});
TweenMax.set(d_fish3, {x: '+=500'});
TweenMax.set(d_fish4, {x: '+=500'});
TweenMax.set(d_fish5, {x: '+=500'});
TweenMax.set(d_fish6, {x: '+=500'});

TweenMax.set(n_fish1, {x: '+=800'});
TweenMax.set(n_fish2, {x: '+=500'});
TweenMax.set(n_fish3, {x: '+=600'});
TweenMax.set(n_fish4, {x: '+=800'});
TweenMax.set(n_fish5, {x: '+=400'});
TweenMax.set(n_fish6, {x: '+=700'});
TweenMax.set(n_fish7, {x: '+=750'});
TweenMax.set(n_fish8, {x: '+=850'});
TweenMax.set(n_fish9, {x: '+=550'});
TweenMax.set(n_fish10, {x: '+=510'});
TweenMax.set(n_fish11, {x: '+=520'});
TweenMax.set(n_fish12, {x: '+=530'});
TweenMax.set(n_fish13, {x: '+=540'});
TweenMax.set(n_fish14, {x: '+=550'});
TweenMax.set(n_fish15, {x: '+=550'});
TweenMax.set(n_fish16, {x: '+=560'});
TweenMax.set(n_fish17, {x: '+=570'});
TweenMax.set(n_fish18, {x: '+=580'});
TweenMax.set(n_fish19, {x: '+=590'});
TweenMax.set(n_fish20, {x: '+=600'});
TweenMax.set(n_fish21, {x: '+=620'});
TweenMax.set(n_fish22, {x: '+=650'});



function moveFishes() {
    moveFish(blueFish1, 150, 3, 80, 6, 'left');
    moveFish(blueFish2, 150, 3, 80, 6, 'left');
    moveFish(blueFish3, 150, 3, 80, 6, 'left');
    moveFish(blueFish4, 150, 3, 80, 6, 'left');

    moveFish(bfish1, 150, 2, 80, 6, 'left');
    moveFish(bfish2, 150, 2, 80, 6, 'left');
    moveFish(bfish3, 150, 2, 80, 6, 'left');
    moveFish(bfish4, 150, 2, 80, 6, 'left');
    moveFish(bfish5, 150, 2, 80, 6, 'left');
    moveFish(bfish6, 150, 2, 80, 6, 'left');
    moveFish(bfish7, 150, 2, 80, 6, 'left');
    moveFish(bfish8, 150, 2, 80, 6, 'left');
    moveFish(bfish9, 150, 2, 80, 6, 'left');

    moveFish(d_fish1, 150, 2, 80, 6, 'left');
    moveFish(d_fish2, 150, 2, 80, 6, 'left');
    moveFish(d_fish3, 150, 2, 80, 6, 'left');
    moveFish(d_fish4, 150, 2, 80, 6, 'left');
    moveFish(d_fish5, 150, 2, 80, 6, 'left');
    moveFish(d_fish6, 150, 2, 80, 6, 'left');

    moveFish(n_fish1, 100, 2, 80, 6, 'left');
    moveFish(n_fish2, 100, 2, 80, 6, 'left');
    moveFish(n_fish3, 100, 2, 80, 6, 'left');
    moveFish(n_fish4, 100, 2, 80, 6, 'left');
    moveFish(n_fish5, 100, 2, 80, 6, 'left');
    moveFish(n_fish6, 100, 2, 80, 6, 'left');
    moveFish(n_fish7, 100, 2, 80, 6, 'left');
    moveFish(n_fish8, 100, 2, 80, 6, 'left');
    moveFish(n_fish9, 100, 2, 80, 6, 'left');
    moveFish(n_fish10, 100, 2, 80, 6, 'left');
    moveFish(n_fish11, 100, 2, 80, 6, 'left');
    moveFish(n_fish12, 100, 2, 80, 6, 'left');
    moveFish(n_fish13, 100, 2, 80, 6, 'left');
    moveFish(n_fish14, 100, 2, 80, 6, 'left');
    moveFish(n_fish15, 100, 2, 80, 6, 'left');
    moveFish(n_fish16, 100, 2, 80, 6, 'left');
    moveFish(n_fish17, 100, 2, 80, 6, 'left');
    moveFish(n_fish18, 100, 2, 80, 6, 'left');
    moveFish(n_fish19, 100, 2, 80, 6, 'left');
    moveFish(n_fish20, 100, 2, 80, 6, 'left');
    moveFish(n_fish21, 100, 2, 80, 6, 'left');
    moveFish(n_fish22, 100, 2, 80, 6, 'left');
    
    
}

function moveFish(fish, step, stepTime, stepNum, deg, direction) {
    var sign = (direction === 'left') ? '-=' : '+=';
    var degAdjustment = (direction === 'left') ? 0 : 180;
    var tl = new TimelineMax({repeat: -1});
    for(var i = 1; i <= stepNum; i++) {
        if(i % 2 === 0) {
            tl.to(fish, stepTime, {x:sign + step, skewY: degAdjustment - deg + 'deg', ease: Power0.easeNone});
        }
        else {
            tl.to(fish, stepTime, {x: sign + step, skewY: degAdjustment + deg + 'deg', ease: Power0.easeNone});
        }
    }
}
function moveFins() {
    var tl = new TimelineMax({repeat: -1, yoyo: true});
    tl.to('.fin', 1, {skewX: '3deg'});
}
var master = new TimelineMax();
master.add(moveFishes)
    .add(moveFins);



if ($(window).width() < 960) {
    $('.full_detail .right h5').click(function(){
        $('.full_detail .right .about_school').slideToggle();
    });
}
else {

}


$('.water_images .Syllabus .behind_pepole .grid_box .box').click(function(){
    var po_co = $(this).find('.pop_text').html();
    $('.popup .popup_wrap .pop_content .po_app').append(po_co);
    $('.popup').addClass('active');
});

$('.popup .bg_color').click(function(){
    $('.popup').removeClass('active');
    setTimeout(function(){
        $('.popup .popup_wrap .pop_content .po_app').empty()
    },100)
});

$('.popup .close_popup').click(function(){
    $('.popup').removeClass('active');
    setTimeout(function(){
        $('.popup .popup_wrap .pop_content .po_app').empty()
    },100)
});









