/* Window Load functions */

$(window).load(function(){
    setTimeout(function(){

    });
});
/*carousel*/
$(document).ready(function(){
   $(".owl-carousel").owlCarousel({
      items:1,
      nav:true,
      loop:true,
      dots:true,
      smartSpeed:1500,
      rtl:true,
   });
});


$(document).ready(function(){
 
     $('.header_wrapper .menu_toggle .hamburger').click(function() {

         $('.outer_menu .mobile_menu').addClass('is-active');
         if($('.outer_menu .mobile_menu').hasClass('is-active'))
         {
           $('.header_wrapper .menu_toggle .hamburger').addClass('is-active');      
           $('.page_slide').addClass('is-active');
           $('header').addClass('is-active');     
         }
         else{
             $('.header_wrapper .menu_toggle .hamburger').addClass('is-active');      
         }
         
         $('.inner_hamburger').click(function() {
             $('.outer_menu .mobile_menu').removeClass('is-active');
             $('.header_wrapper .menu_toggle .hamburger').removeClass('is-active');
             $('.page_slide').removeClass('is-active'); 
             $('header').removeClass('is-active');     
         });          
     });
        
     /* on click of anchor smooth scroll slide menu */    
     $('.outer_menu .mobile_menu a.scroll_btn').click(function(e){
         e.preventDefault();
         var a = $(this).attr('href');

         $('html,body').animate({
           scrollTop: $(a).offset().top
         },800);    
    });    
    
    /* header fix */
     $(window).scroll(function(){
        if ($(window).scrollTop()) {
            $('header').addClass('sticky');
        }
        else {
            $('header').removeClass('sticky');
        }
    });
    /*  parallax */
    $(window).scroll(function () {
        $('.banner').css("background-position","50% " + ($(this).scrollTop() / 2) + "px");
    
       
    });

});

$(window).resize(function(){

})


/* parallax bottom-banner */
;(function($) {
    $window = $(window);

    $('*[data-type="parallax"]').each(function(){

        var $bgobj = $(this);

        $(window).scroll(function() {

            var yPos = -($window.scrollTop() / $bgobj.data('speed'));
            var coords = '50% '+ yPos + 'px';

            $bgobj.css({ backgroundPosition: coords });

        });
    });
})(jQuery);


