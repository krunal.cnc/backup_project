/* Window Load functions */

$(window).on('load',function(){
    setTimeout(function(){

    });
});


$(document).ready(function(){

});

$(window).resize(function(){

})

$(".light .main .banner .hero_img").vegas({
    slides: [
        { src: "./images/site/banner.jpg" },
    ],
    overlay: './vegas/overlays/05.png'
});

$(".dark .main .hero_img").vegas({
    slides: [
        { src: "./images/site/dark_banner.jpg" },
    ],
    overlay: './vegas/overlays/05.png'
});
/* achievement */
equalheight = function(container){
    var currentTallest = 0,
        currentRowStart = 0,
        rowDivs = new Array(),
        $el,
        topPosition = 0;
    $(container).each(function() {

        $el = $(this);
        $($el).height('auto')
        topPostion = $el.position().top;

        if (currentRowStart != topPostion) {
            for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
            rowDivs.length = 0; // empty the array
            currentRowStart = topPostion;
            currentTallest = $el.height();
            rowDivs.push($el);
        } else {
            rowDivs.push($el);
            currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
        }
        for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
            rowDivs[currentDiv].height(currentTallest);
        }
    });
}

$(window).load(function() {
    equalheight('.select_event .event_box .blog');
});


$(window).resize(function(){
    equalheight('.select_event .event_box .blog');
});


/*sticky*/
$(document).ready(function(){
    $(window).scroll(function(){
        if ($(window).scrollTop() >= 200) {
            $('header').addClass('sticky');
        }
        else {
            $('header').removeClass('sticky');

        }
    });
});

/* jquery */
$(document).ready(function(e){
    (function ($) {
        $(".success_story_content").owlCarousel({
            loop:true,
            margin:10,
            nav : true,
            dots:true,
            center:false,
            rtl:true,
            items:3,
            responsiveClass:true, 
            responsive:{
                0:{
                    items:1,
                    margin:0,
                },
                768:{
                    items:2,
                },
                1024:{
                    items:3,
                }
            }
        });        

        $("#about_c").owlCarousel({
            loop:true,
            margin:10,
            dots:true,
            center:false,
            rtl:true,
            items:1,
            responsiveClass:true, 
            responsive:{
                0:{
                    items:1,
                    margin:0,
                },
                768:{
                    items:1,
                },
                1024:{
                    items:1,
                }
            }
        });        
    })(jQuery);

});
/* shadobox */
Shadowbox.init({
    language: 'en',
    players: ['img', 'html', 'iframe', 'qt', 'wmp', 'swf', 'flv']
});
/* mobile toggle */
/* menu toggle */
$('.hamburger').click(function () {
    $(this).toggleClass('is-active'); 
    $('.mobile_menu').toggleClass('is-active');   
});



/*  */
/*(function($) {
    $.fn.visible = function(partial) {
        var $t            = $(this),
            $w            = $(window),
            viewTop       = $w.scrollTop(),
            viewBottom    = viewTop + $w.height(),
            _top          = $t.offset().top,
            _bottom       = _top + $t.height(),
            compareTop    = partial === true ? _bottom : _top,
            compareBottom = partial === true ? _top : _bottom;

        return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
    };
})(jQuery);
var win = $(window);
var had = $("header a");
var allMods = $(".select_event .paint .top_left_img");

win.scroll(function(event) {
    allMods.each(function(i, el) {
        var el = $(el);
        if (el.visible(true)) {
            setTimeout(function(){
                el.addClass("come-in"); 
            },i*200);
        } 
    });
});*/






