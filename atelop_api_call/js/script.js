/* Window Load functions */

$(window).on('load',function(){
    setTimeout(function(){
        $('.loading').fadeOut(100);
    },500);
    setTimeout(function(){
        $('.banner .left p').addClass('show');
        $('.banner .left .dropdown .drp').addClass('show');
        $('.banner .right img').attr('src','images/site/gif1.gif');

        var myLoadingTimer = setTimeout(function(){
            $('.banner .right img').attr('src','images/site/gif2.gif');    
        },2700);        

    },1500);
});



$(window).resize(function(){

})

$(document).ready(function(){

    if ($(window).width() > 767) {
        $('.client .client_slider').owlCarousel({
            loop:true,                   
            nav : true,
            items:4,   
            responsiveClass:true,
            smartSpeed:450,
            autoplay:true,
            autoplayTimeout:2000,
            responsive:{
                0:{
                    items:1,
                },
                768:{
                    items:2,
                },
                1024:{
                    items:3,
                },
                1280:{
                    items:3,
                }, 
                1281:{
                    items:4,
                },        
                1366:{
                    items:4,
                }
            }
        }); 
    }
    else {

    }

    /* chaechbox */
    $('label input[type=radio], label input[type=checkbox]').click(function() {
        $('label:has(input:checked)').addClass('active');
        $('label:has(input:not(:checked))').removeClass('active');
    });
    /* custome drop */  
    $('.banner .left .dropdown .drp .drp_val').click(function(){
        $(this).toggleClass('active');
        $('.banner .left .dropdown .drp .list_brands').slideToggle();
    });
    $('.banner .left .dropdown .drp .list_brands ul li a').click(function(){
        $(this).removeClass('active');
        $('.banner .left .dropdown .drp .list_brands').slideUp();
        $('.banner .left .dropdown .drp .drp_val').removeClass('active');
    });

    /* tab */
    jQuery(window).on('load',function(){
        $('.tab_container .tab_content').eq(0).addClass('active')
        $('.tab_container').outerHeight($('.tab_container .tab_content').eq(0).outerHeight())
    });
    jQuery('.detect_content .tab_main ul li').click(function(){
        var i = $(this).index();
        $(this).addClass('active').siblings().removeClass('active')
        $('.tab_container .tab_content').removeClass('active')
        $('.tab_container .tab_content').eq(i).addClass('active')
        $('.tab_container').outerHeight($('.tab_container .tab_content').eq(i).outerHeight())
    });



});




function sticky_header(){
    var scroll = $(window).scrollTop();    
    if (scroll > 10) {
        $("header").addClass("sticky");
        $(".main").addClass("sticky");
    }
    else {
        $("header").removeClass("sticky");
        $(".main").removeClass("sticky");
    }
}
$(window).on('load',sticky_header);
$(window).scroll(sticky_header);



/* menu toggle */
jQuery('.hamburger').click(function () {
    jQuery('.hamburger').toggleClass('is-active'); 
    jQuery('header .menu').toggleClass('is-active');   
}); 

jQuery( "header nav ul li a" ).on( "click", function() {
    jQuery('.hamburger').removeClass('is-active'); 
    jQuery('header .menu').removeClass('is-active');
});


/* mobile scroll */
if($(window).width() < 768 )
{
    $('.detect_content .tab_main ul').css('width',(parseInt($('.detect_content .tab_main ul li').css('width')) * $('.detect_content .tab_main ul li').length));

    /* video */
    var _v_w_ = parseInt($('.detect_content .tab_main .tab_container #tab-2.tab_content .video_anchor').css('width'));
    var _v_l_ = $('.detect_content .tab_main .tab_container #tab-2.tab_content .video_anchor').length
    var v_mr = parseInt($('.detect_content .tab_main .tab_container #tab-2.tab_content .video_anchor').css('margin-right'));
    var _v_new_ = v_mr + _v_w_;    
    $('.detect_content .tab_main .tab_container #tab-2.tab_content .video').css('width',(_v_new_ * _v_l_));

    /* image */
    var _i_w_ = parseInt($('.detect_content .tab_main #tab-1.tab_content .video_anchor').css('width'));
    var _i_l_ = $('.detect_content .tab_main #tab-1.tab_content .video_anchor').length
    var i_mr = parseInt($('.detect_content .tab_main #tab-1.tab_content .video_anchor').css('margin-right'));
    var _i_new_ = i_mr + _i_w_;    
    $('.detect_content .tab_main #tab-1.tab_content .video').css('width',(_i_new_ * _i_l_));

    /*article*/
    var _a_w_ = parseInt($('.detect_content .tab_main .tab_container .tab_content .article .box').css('width'));
    var _a_l_ = $('.detect_content .tab_main .tab_container #tab-3.tab_content .article .box').length
    var a_mr = parseInt($('.detect_content .tab_main .tab_container .tab_content .article .box').css('margin-right'));
    var _a_new_ = a_mr + _a_w_;
    $('.detect_content .tab_main .tab_container .tab_content .article').css('width',(_a_new_ * _a_l_));

    /*promotional*/
    var _p_w_ = parseInt($('.detect_content .tab_main .tab_container .tab_content .article .box').css('width'));
    var _p_l_ = $('.detect_content .tab_main .tab_container #tab-4.tab_content .article .box').length
    var p_mr = parseInt($('.detect_content .tab_main .tab_container .tab_content .article .box').css('margin-right'));
    var _p_new_ = p_mr + _p_w_;
    $('.detect_content .tab_main .tab_container .tab_content .article').css('width',(_p_new_ * _p_l_));

}
else{


    $(document).on('click','.detect_content .tab_main .tab_container #tab-2 .load_more',function () {
        $(this).addClass('load_class');
        setTimeout(function(){
            $('.detect_content .tab_main .tab_container #tab-2.tab_content .video_anchor:hidden').slice(0, 6).show();
        },700)
        setTimeout(function(){
            $('.tab_container').outerHeight($('.tab_container #tab-2.tab_content').eq(0).outerHeight())
            $('.detect_content .tab_main .tab_container #tab-2 .load_more').removeClass('load_class');
        },800)

        if ($('.detect_content .tab_main .tab_container #tab-2.tab_content .video_anchor').length == $('.detect_content .tab_main .tab_container #tab-2.tab_content .video_anchor:visible').length) {
            $(this).hide();
        }
    });


    $(document).on('click','.detect_content .tab_main .tab_container #tab-1 .load_more',function () {
        $(this).addClass('load_class');
        setTimeout(function(){
            $('.detect_content .tab_main .tab_container #tab-1.tab_content .video_anchor:hidden').slice(0, 6).show();
        },700)
        setTimeout(function(){
            $('.tab_container').outerHeight($('.tab_container #tab-1.tab_content').eq(0).outerHeight())
            $('.detect_content .tab_main .tab_container #tab-1  .load_more').removeClass('load_class');
        },800)

        if ($('.detect_content .tab_main .tab_container #tab-1.tab_content .video_anchor').length == $('.detect_content .tab_main .tab_container #tab-2.tab_content .video_anchor:visible').length) {
            $(this).hide();
        }
    });

    $(document).on('click','.detect_content .tab_main .tab_container #tab-3 .load_more',function () {
        $(this).addClass('load_class');
        setTimeout(function(){ 
            $('.detect_content .tab_main .tab_container #tab-3.tab_content .article .box:hidden').slice(0, 3).show();
        },700)
        setTimeout(function(){
            $('.tab_container').outerHeight($('.tab_container #tab-3.tab_content').eq(0).outerHeight());
            $('.detect_content .tab_main .tab_container #tab-3 .load_more').removeClass('load_class');
        },800)

        if ($('.detect_content .tab_main .tab_container #tab-3.tab_content .article .box').length == $('.detect_content .tab_main .tab_container #tab-3.tab_content .article .box:visible').length) {
            $(this).hide();
        }
    });

    $(document).on('click','.detect_content .tab_main .tab_container #tab-4 .load_more',function () {
        $(this).addClass('load_class');
        setTimeout(function(){ 
            $('.detect_content .tab_main .tab_container #tab-4.tab_content .article .box:hidden').slice(0, 3).show();
        },700)
        setTimeout(function(){
            $('.tab_container').outerHeight($('.tab_container #tab-4.tab_content').eq(0).outerHeight());
            $('.detect_content .tab_main .tab_container #tab-4 .load_more').removeClass('load_class');
        },800)

        if ($('.detect_content .tab_main .tab_container #tab-4.tab_content .article .box').length == $('.detect_content .tab_main .tab_container #tab-4.tab_content .article .box:visible').length) {
            $(this).hide();
        }
    });



}


$(window).load(function(){

});



(function($) {
    $.fn.visible = function(partial) {
        var $t            = $(this),
            $w            = $(window),
            viewTop       = $w.scrollTop(),
            viewBottom    = viewTop + $w.height(),
            _top          = $t.offset().top,
            _bottom       = _top + $t.height(),
            compareTop    = partial === true ? _bottom : _top,
            compareBottom = partial === true ? _top : _bottom;

        return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
    };
})(jQuery);
var win = $(window);
var allMods = $(".detect_content .content_type h3");
var allMods2 = $(".detect_content .content_type p");
var allMods3 = $(".our_service .service_div .left_sec .left");
var allMods4 = $(".our_service .service_div .left_sec .right h3");
var allMods5 = $(".our_service .service_div .left_sec .right p");
var allMods6 = $(".our_service .service_div .right_sec .right");
var allMods7 = $(".our_service .service_div .right_sec .left h3");
var allMods8 = $(".our_service .service_div .right_sec .left p");
win.scroll(function(event) {
    allMods.each(function(i, el) {
        var el = $(el);
        if (el.visible(true)) {
            setTimeout(function(){
                el.addClass("come-in");                 
            },i*200);
        } 
    });
});
win.scroll(function(event) {
    allMods2.each(function(i, el) {
        var el = $(el);
        if (el.visible(true)) {
            setTimeout(function(){
                el.addClass("come-in");                 
            },i*200);
        } 
    });
});
win.scroll(function(event) {
    allMods3.each(function(i, el) {
        var el = $(el);
        if (el.visible(true)) {
            setTimeout(function(){
                el.addClass("come-in");                 
            },i*200);
        } 
    });
});
win.scroll(function(event) {
    allMods4.each(function(i, el) {
        var el = $(el);
        if (el.visible(true)) {
            setTimeout(function(){
                el.addClass("come-in");                 
            },i*200);
        } 
    });
});
win.scroll(function(event) {
    allMods5.each(function(i, el) {
        var el = $(el);
        if (el.visible(true)) {
            setTimeout(function(){
                el.addClass("come-in");                 
            },i*200);
        } 
    });
});
win.scroll(function(event) {
    allMods6.each(function(i, el) {
        var el = $(el);
        if (el.visible(true)) {
            setTimeout(function(){
                el.addClass("come-in");                 
            },i*200);
        } 
    });
});
win.scroll(function(event) {
    allMods7.each(function(i, el) {
        var el = $(el);
        if (el.visible(true)) {
            setTimeout(function(){
                el.addClass("come-in");                 
            },i*200);
        } 
    });
});
win.scroll(function(event) {
    allMods8.each(function(i, el) {
        var el = $(el);
        if (el.visible(true)) {
            setTimeout(function(){
                el.addClass("come-in");                 
            },i*200);
        } 
    });
});




$(document).ready(function () {

    $(document).on("scroll", onScroll);
    $('header nav ul li a[href^="#"]').on('click', function (e) {
        e.preventDefault();
        $(document).off("scroll");

        $('header nav ul li a').each(function () {
            $(this).removeClass('active');
            $(this).parents('li').removeClass('active');
        })
        $(this).addClass('active');
        $(this).parents('li').addClass('active');

        var target = this.hash,
            menu = target;
        $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top - $('header').height()
        }, 500, 'swing', function () {
            $(document).on("scroll", onScroll);
        });
    });

});

function onScroll(event){

    var scrollPos = $(document).scrollTop();
    $('header nav ul li a').each(function () {
        var currLink = $(this);
        var refElement = $(currLink.attr("href"));
        if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
            $('header nav ul li a').removeClass("active");

            $('header nav ul li a').removeClass("active");    
            currLink.addClass("active");
            currLink.addClass("active");
        }
        else{
            currLink.removeClass("active");
            currLink.removeClass("active");
        }
    });
}


$('.slider').slick({
    vertical: true,
    autoplay: true,
    autoplaySpeed: 3000,
    speed: 300,
    prevArrow: null,
    nextArrow: null,
});

(function($){
    jQuery(window).on("load",function(){
        jQuery(".inner_part").mCustomScrollbar(); 
    });
})(jQuery);

$(document).on('click','.detect_content .tab_main .tab_container .tab_content .video_anchor',function(){
    $('.content_popup').addClass('active'); 
});
$('.content_popup .left_part .close_btn').click(function(){
    $('.content_popup').removeClass('active');  
});



